use bytes::Bytes;
use rdanbooru::{
    apis::{
        artist_commentaries::GetArtistCommentaryByPostIdBuilder,
        autocomplete::GetAutoCompleteBuilder,
        favorites::{
            DeleteFavoriteByPostIdBuilder, GetFavoritesBuilder, PostFavoriteByPostIdBuilder,
        },
        file::GetFileBuilder,
        get_profile,
        post::{GetPostByIdBuilder, GetPostsBuilder},
        user::GetProfileBuilder,
    },
    clients::Client,
    models::{
        artist_commentary::ArtistCommentary, auth::Auth, autocomplete::AutoComplete,
        favorite::Favorite, post::Post, user::Profile,
    },
    ClientError,
};
use tokio::task::JoinHandle;
use xtra::Handler;

use super::{RemoteManager, Resolvable};

impl Resolvable<Client, Vec<Post>, ClientError> for GetPostsBuilder {
    async fn resolve(&self, client: &Client) -> Result<Vec<Post>, ClientError> {
        self.send(client).await
    }
}

impl Resolvable<Client, Post, ClientError> for GetPostByIdBuilder {
    async fn resolve(&self, client: &Client) -> Result<Post, ClientError> {
        self.send(client).await
    }
}

impl Resolvable<Client, Bytes, ClientError> for GetFileBuilder {
    async fn resolve(&self, client: &Client) -> Result<Bytes, ClientError> {
        self.send(client).await
    }
}

impl Resolvable<Client, ArtistCommentary, ClientError> for GetArtistCommentaryByPostIdBuilder {
    async fn resolve(&self, client: &Client) -> Result<ArtistCommentary, ClientError> {
        self.send(client).await
    }
}

impl Resolvable<Client, Vec<AutoComplete>, ClientError> for GetAutoCompleteBuilder {
    async fn resolve(&self, client: &Client) -> Result<Vec<AutoComplete>, ClientError> {
        self.send(client).await
    }
}

impl Resolvable<Client, Vec<Favorite>, ClientError> for GetFavoritesBuilder {
    async fn resolve(&self, client: &Client) -> Result<Vec<Favorite>, ClientError> {
        self.send(client).await
    }
}

impl Resolvable<Client, Post, ClientError> for PostFavoriteByPostIdBuilder {
    async fn resolve(&self, client: &Client) -> Result<Post, ClientError> {
        self.send(client).await
    }
}

impl Resolvable<Client, (), ClientError> for DeleteFavoriteByPostIdBuilder {
    async fn resolve(&self, client: &Client) -> Result<(), ClientError> {
        self.send(client).await
    }
}

impl Resolvable<Client, Profile, ClientError> for GetProfileBuilder {
    async fn resolve(&self, client: &Client) -> Result<Profile, ClientError> {
        self.send(client).await
    }
}

pub struct Login(pub Auth);
pub struct LoginSuccess(Client);

impl Handler<Login> for RemoteManager {
    type Return = JoinHandle<Result<Profile, ClientError>>;

    async fn handle(&mut self, Login(auth): Login, ctx: &mut xtra::Context<Self>) -> Self::Return {
        let new_client = {
            let rdanbooru_client = self.rdanbooru_client.read().await;

            rdanbooru::clients::Client::from_config(rdanbooru::Config {
                auth: Some(auth),
                ..rdanbooru_client.get_config().clone()
            })
        };

        let addr = ctx.mailbox().address();

        tokio::spawn(async move {
            match get_profile().send(&new_client).await {
                Ok(profile) => {
                    addr.send(LoginSuccess(new_client))
                        .await
                        .expect("Request manager should not be dropped");

                    Ok(profile)
                }
                Err(err) => Err(err),
            }
        })
    }
}

impl Handler<LoginSuccess> for RemoteManager {
    type Return = ();

    async fn handle(
        &mut self,
        LoginSuccess(new_client): LoginSuccess,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let mut guard = self.rdanbooru_client.write().await;
        *guard = new_client;
    }
}
