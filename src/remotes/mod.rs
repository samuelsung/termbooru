use std::{marker::PhantomData, sync::Arc};

use tokio::sync::RwLock;
use xdg::BaseDirectories;

pub mod danbooru;
pub mod localstorage;

#[derive(xtra::Actor)]
pub struct RemoteManager {
    rdanbooru_client: Arc<RwLock<rdanbooru::clients::Client>>,
    xdg_dirs: Option<Arc<BaseDirectories>>,
}

impl Default for RemoteManager {
    fn default() -> Self {
        Self::new()
    }
}

impl RemoteManager {
    pub fn new() -> Self {
        let rdanbooru_client = rdanbooru::clients::Client::from_config(rdanbooru::Config {
            // base_url: if args.safe {
            //     "https://safebooru.donmai.us/".to_owned()
            // } else {
            base_url: "https://danbooru.donmai.us/".to_owned(),
            auth: None,
        });

        let xdg_dirs = xdg::BaseDirectories::with_prefix("termbooru") // TODO(samuelsung): error handling
            .ok()
            .map(Into::into);

        Self {
            rdanbooru_client: Arc::new(RwLock::new(rdanbooru_client)),
            xdg_dirs,
        }
    }
}

pub struct Request<Id, Res, Err>(Id, PhantomData<(Res, Err)>);

impl<Id, Res, Err> Request<Id, Res, Err> {
    pub fn new(id: Id) -> Self {
        Self(id, PhantomData)
    }

    pub fn id(self) -> Id {
        self.0
    }
}

impl<Id, Res, Err> From<Id> for Request<Id, Res, Err> {
    fn from(value: Id) -> Self {
        Self::new(value)
    }
}

pub trait Resolvable<Client, Res, Err>
where
    Self: Send + 'static,
    Res: Send + 'static,
    Err: Send + 'static,
{
    fn resolve(
        &self,
        client: &Client,
    ) -> impl std::future::Future<Output = Result<Res, Err>> + Send;
}

impl<Id, Res, Err> xtra::Handler<Request<Id, Res, Err>> for RemoteManager
where
    Id: Resolvable<rdanbooru::clients::Client, Res, Err>,
    Err: std::marker::Send + 'static,
    Res: std::marker::Send + 'static,
{
    type Return = tokio::task::JoinHandle<Result<Res, Err>>;

    async fn handle(
        &mut self,
        message: Request<Id, Res, Err>,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let rdanbooru_client = self.rdanbooru_client.clone();

        tokio::spawn(async move {
            let rdanbooru_client = rdanbooru_client.read().await;
            message.id().resolve(&*rdanbooru_client).await
        })
    }
}
