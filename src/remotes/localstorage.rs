use std::{fs::remove_file, io};

use rdanbooru::models::auth::Auth;
use tokio::task::JoinHandle;
use xtra::Handler;

use super::RemoteManager;

pub static AUTH_JSON: &str = "auth.json";

#[derive(Debug, thiserror::Error)]
pub enum RwLoginError {
    #[error("failed to read/write")]
    IO(#[from] io::Error),

    #[error("{0} not exist")]
    NotExist(&'static str),

    #[error("failed to read/write json")]
    Serde(#[from] serde_json::Error),
}

pub struct ReadLogin;

impl Handler<ReadLogin> for RemoteManager {
    type Return = JoinHandle<Result<Auth, RwLoginError>>;

    async fn handle(
        &mut self,
        _message: ReadLogin,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let xdg_dirs = self.xdg_dirs.clone();

        tokio::task::spawn_blocking(move || {
            let Some(xdg_dirs) = xdg_dirs else {
                return Err(RwLoginError::NotExist(AUTH_JSON));
            };

            let config_file = xdg_dirs
                .find_config_file(AUTH_JSON)
                .ok_or(RwLoginError::NotExist(AUTH_JSON))?;
            let file = std::fs::File::open(config_file)?;
            let reader = io::BufReader::new(file);
            Ok(serde_json::from_reader::<_, Auth>(reader)?)
        })
    }
}

pub struct SaveLogin(pub Auth);

impl Handler<SaveLogin> for RemoteManager {
    type Return = JoinHandle<Result<Auth, RwLoginError>>;

    async fn handle(
        &mut self,
        SaveLogin(auth): SaveLogin,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let xdg_dirs = self.xdg_dirs.clone();

        tokio::task::spawn_blocking(move || {
            let Some(xdg_dirs) = xdg_dirs else {
                return Err(RwLoginError::NotExist(AUTH_JSON));
            };

            let config_file = xdg_dirs.place_config_file(AUTH_JSON)?;
            if let Some(parent) = config_file.parent() {
                std::fs::create_dir_all(parent)?;
            }
            let file = std::fs::File::create(config_file)?;
            let writer = io::BufWriter::new(file);
            serde_json::to_writer_pretty(writer, &auth)?;

            Ok(auth)
        })
    }
}

pub struct RemoveLogin;

impl Handler<RemoveLogin> for RemoteManager {
    type Return = JoinHandle<Result<(), RwLoginError>>;

    async fn handle(
        &mut self,
        _message: RemoveLogin,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let xdg_dirs = self.xdg_dirs.clone();

        tokio::task::spawn_blocking(move || {
            let Some(xdg_dirs) = xdg_dirs else {
                return Err(RwLoginError::NotExist(AUTH_JSON));
            };

            let config_file = xdg_dirs
                .find_config_file(AUTH_JSON)
                .ok_or(RwLoginError::NotExist(AUTH_JSON))?;

            remove_file(config_file)?;
            Ok(())
        })
    }
}
