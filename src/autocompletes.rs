use ratatui::{style::Stylize, text::Span};
use rdanbooru::{
    apis::{autocomplete::AutoCompleteSearchType, get_autocomplete},
    models::autocomplete::AutoComplete,
    ClientError,
};
use tracing::debug;

use crate::{
    misc::{pool_color, tag_color, user_level_color, HumanizedNumber},
    remotes::Request,
    tui::Ctx,
};

#[derive(Debug)]
pub enum AutoCompleteReqMode {
    New,
    AppendLast,
}

#[derive(Debug)]
pub struct AutoCompleteReq {
    pub tokens: Vec<String>,
    pub mode: AutoCompleteReqMode,
}

#[derive(Debug)]
pub struct AutoCompleteSuggestion {
    pub text: String,
    pub display: Vec<Span<'static>>,
}

pub type AutoCompleteRes = Vec<AutoCompleteSuggestion>;

#[derive(Debug, thiserror::Error)]
pub enum AutoCompleteErr {
    #[error("error when fetching {0:?}: {1:?}")]
    FetchError(Option<String>, ClientError),

    #[error("unsupported for this command")]
    Unsupported,
}

impl AutoCompleteReq {
    #[tracing::instrument(skip(ctx))]
    pub async fn resolve_tag(self, ctx: &Ctx) -> Result<AutoCompleteRes, AutoCompleteErr> {
        let res = {
            let mut builder = get_autocomplete()
                .limit(10)
                .version(1)
                .search_type(AutoCompleteSearchType::TagQuery);

            let last = self.tokens.last();

            if let (Some(last), AutoCompleteReqMode::AppendLast) = (last, self.mode) {
                builder = builder.search_query(last.to_owned())
            }

            ctx.remote_manager_address
                .send(Request::new(builder))
                .await
                .expect("Request manager should not be dropped")
                .await
                .expect("Tokio spawn panic")
                .map_err(|e| AutoCompleteErr::FetchError(last.cloned(), e))?
        };

        debug!("res: {res:?}");

        let autocompletes =
            res.into_iter()
                .map(|autocomplete| match autocomplete {
                    AutoComplete::Emoji { label, value }
                    | AutoComplete::Static { label, value } => AutoCompleteSuggestion {
                        text: value,
                        display: vec![label.into()],
                    },

                    AutoComplete::Mention {
                        label,
                        value,
                        level,
                        ..
                    }
                    | AutoComplete::User {
                        label,
                        value,
                        level,
                        ..
                    } => AutoCompleteSuggestion {
                        text: value,
                        display: vec![Span::from(label).fg(user_level_color(&level))],
                    },

                    AutoComplete::Pool {
                        label,
                        value,
                        post_count,
                        category,
                        ..
                    } => AutoCompleteSuggestion {
                        text: value,
                        display: vec![
                            Span::from(label).fg(pool_color(&category)),
                            format!(" {}", HumanizedNumber::from(post_count)).into(),
                        ],
                    },

                    AutoComplete::TagAbbreviation {
                        label,
                        value,
                        category,
                        post_count,
                        antecedent,
                    }
                    | AutoComplete::TagAlias {
                        label,
                        value,
                        category,
                        post_count,
                        antecedent,
                    }
                    | AutoComplete::TagAutocorrect {
                        label,
                        value,
                        category,
                        post_count,
                        antecedent,
                    }
                    | AutoComplete::TagOtherName {
                        label,
                        value,
                        category,
                        post_count,
                        antecedent,
                    }
                    | AutoComplete::TagWord {
                        label,
                        value,
                        category,
                        post_count,
                        antecedent: Some(antecedent),
                    } => {
                        let fg_color = tag_color(&category);
                        AutoCompleteSuggestion {
                            text: value,
                            display: vec![
                                Span::from(antecedent).fg(fg_color),
                                " -> ".into(),
                                Span::from(label).fg(fg_color),
                                format!(" {}", HumanizedNumber::from(post_count)).into(),
                            ],
                        }
                    }

                    AutoComplete::TagWord {
                        label,
                        value,
                        category,
                        post_count,
                        antecedent: None,
                    }
                    | AutoComplete::Tag {
                        label,
                        value,
                        category,
                        post_count,
                    } => AutoCompleteSuggestion {
                        text: value,
                        display: vec![
                            Span::from(label).fg(tag_color(&category)),
                            format!(" {}", HumanizedNumber::from(post_count)).into(),
                        ],
                    },
                })
                .collect();

        debug!("autocomplete: {autocompletes:?}");

        Ok(autocompletes)
    }

    #[tracing::instrument(skip(ctx))]
    pub async fn resolve(self, ctx: &Ctx) -> Result<AutoCompleteRes, AutoCompleteErr> {
        let mut tokens = self.tokens.into_iter();

        match tokens.next().as_deref() {
            Some("posts") => {
                Self {
                    tokens: tokens.collect(),
                    ..self
                }
                .resolve_tag(ctx)
                .await
            }
            _ => Err(AutoCompleteErr::Unsupported),
        }
    }
}
