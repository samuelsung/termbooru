use std::{fmt, io};

use derive_debug::Dbg;
use ratatui::{
    prelude::*,
    symbols::border,
    widgets::{block::*, *},
};
use rdanbooru::{
    apis::{get_artist_commentary_by_post_id, get_favorites, get_file, get_post_by_id},
    models::{artist_commentary::ArtistCommentary, post::Post, NumberField},
    ClientError,
};

use crate::{
    remotes::Request,
    tui::{
        image::{AsyncStatefulImage, AsyncStatefulProtocol},
        pages::{PageId, PageMessage},
        Ctx, SubHandler,
    },
};

impl fmt::Debug for PostState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("PostState")
            .field("page_id", &self.page_id)
            .field("post.id", &self.post.id)
            .field("split_ratio", &self.split_ratio)
            .finish_non_exhaustive()
    }
}

pub struct PostState {
    pub page_id: PageId,
    pub post: Post,
    pub artist_commentary: Option<ArtistCommentary>,
    pub is_favorited: bool,
    pub image_protocol: Option<AsyncStatefulProtocol>,
    pub split_ratio: u16,
}

#[derive(Dbg, Default)]
pub struct PostStateBuilder {
    post_id: Option<usize>,
    post: Option<Post>,
    artist_commentary: Option<Option<ArtistCommentary>>,
    image_protocol: Option<Option<AsyncStatefulProtocol>>,
    preview_image_protocol: Option<AsyncStatefulProtocol>,
    split_ratio: Option<u16>,
    sync_fetch_image: bool,
}

#[derive(Debug, thiserror::Error)]
pub enum PostStateBuildError {
    #[error("missing either post_id or post")]
    PostMissing,

    #[error("failed to read/write")]
    IO(#[from] io::Error),

    #[error("error during fetch post")]
    FetchPost(#[source] ClientError),

    #[error("failed to read image")]
    ReadImage(#[from] image::ImageError),
}

impl PostStateBuilder {
    #[allow(dead_code)]
    pub fn get_post_id(&self) -> Option<usize> {
        if let Some(post) = &self.post {
            Some(post.id)
        } else {
            self.post_id
        }
    }

    #[allow(dead_code)]
    pub fn post_id(mut self, value: usize) -> Self {
        self.post_id = Some(value);
        if self.post.as_ref().is_some_and(|p| p.id != value) {
            self.post = None;
        }
        self
    }

    #[allow(dead_code)]
    pub fn post(mut self, value: Post) -> Self {
        self.post_id = Some(value.id);
        self.post = Some(value);
        self
    }

    #[allow(dead_code)]
    pub fn artist_commentary(mut self, value: Option<ArtistCommentary>) -> Self {
        self.artist_commentary = Some(value);
        self
    }

    #[allow(dead_code)]
    pub fn image_protocol(mut self, value: Option<AsyncStatefulProtocol>) -> Self {
        self.image_protocol = Some(value);
        self
    }

    #[allow(dead_code)]
    pub fn preview_image_protocol(mut self, value: AsyncStatefulProtocol) -> Self {
        self.preview_image_protocol = Some(value);
        self.sync_fetch_image = false;
        self
    }

    #[allow(dead_code)]
    pub fn split_ratio(mut self, value: u16) -> Self {
        self.split_ratio = Some(value);
        self
    }

    #[allow(dead_code)]
    pub fn sync_fetch_image(mut self, value: bool) -> Self {
        self.sync_fetch_image = value;
        if self.sync_fetch_image {
            self.preview_image_protocol = None;
        }
        self
    }

    pub async fn build(
        self,
        user_id: Option<usize>,
        ctx: &Ctx,
    ) -> Result<PostState, PostStateBuildError> {
        let PostStateBuilder {
            post_id,
            post,
            artist_commentary,
            image_protocol,
            preview_image_protocol,
            split_ratio,
            sync_fetch_image,
        } = self;

        let page_id = ctx.page_id_generator.gen_id();

        let post = if let Some(post) = post {
            post
        } else if let Some(id) = post_id {
            ctx.remote_manager_address
                .send(Request::from(get_post_by_id(id)))
                .await
                .expect("Request manager should not be dropped")
                .await
                .expect("Tokio spawn panic")
                .map_err(PostStateBuildError::FetchPost)?
        } else {
            return Err(PostStateBuildError::PostMissing);
        };

        let artist_commentary = if let Some(artist_commentary) = artist_commentary {
            artist_commentary
        } else {
            ctx.remote_manager_address
                .send(Request::from(get_artist_commentary_by_post_id(post.id)))
                .await
                .expect("Request manager should not be dropped")
                .await
                .expect("Tokio spawn panic")
                .ok()
        };

        let is_favorited = if let Some(user_id) = user_id {
            ctx.remote_manager_address
                .send(Request::from(
                    get_favorites()
                        .user_id(NumberField::Eq(user_id))
                        .post_id(NumberField::Eq(post.id)),
                ))
                .await
                .expect("Request manager should not be dropped")
                .await
                .expect("Tokio spawn panic")
                .ok()
                .map(|v| !v.is_empty())
                .unwrap_or(false)
        } else {
            false
        };

        let (need_fetching, mut image_protocol) = if let Some(image_protocol) = image_protocol {
            (false, image_protocol)
        } else if let Some(preview_image_protocol) = preview_image_protocol {
            (true, Some(preview_image_protocol))
        } else {
            (true, None)
        };

        if need_fetching {
            if let Some(url) = &post.file_url {
                let url = url.clone();

                let task = {
                    let ctx = ctx.clone();
                    async move {
                        let file = ctx
                            .remote_manager_address
                            .send(Request::from(get_file(url)))
                            .await
                            .expect("Request manager should not be dropped")
                            .await
                            .expect("Tokio spawn panic")
                            .map_err(PostStateBuildError::FetchPost)?;

                        let image_protocol = image::ImageReader::new(io::Cursor::new(file))
                            .with_guessed_format()
                            .map_err(PostStateBuildError::from)
                            .and_then(|f| Ok(f.decode()?))?;

                        let protocol = AsyncStatefulProtocol::new(
                            ctx.render_manager.clone(),
                            ctx.image_picker.new_resize_protocol(image_protocol),
                        );

                        Ok::<_, PostStateBuildError>(protocol)
                    }
                };

                if sync_fetch_image {
                    image_protocol = Some(task.await?);
                } else {
                    let tui_address = ctx.tui_address.clone();
                    tokio::spawn(async move {
                        let image_protocol = task.await;

                        if let Ok(image_protocol) = image_protocol {
                            let _ = tui_address
                                .send(PageMessage::post(page_id, LoadImage(image_protocol)))
                                .detach()
                                .await;
                        }
                    });
                }
            }
        }

        let split_ratio = split_ratio.unwrap_or(55);

        Ok(PostState {
            page_id,
            post,
            artist_commentary,
            image_protocol,
            split_ratio,
            is_favorited,
        })
    }
}

impl PostState {
    pub fn page_info(&self) -> String {
        format!("post: {}", self.post.id)
    }

    #[tracing::instrument(skip(frame, area))]
    pub fn render(&mut self, frame: &mut Frame, area: Rect) {
        tracing::trace!("start rendering post");
        let layout = Layout::default()
            .direction(Direction::Horizontal)
            .constraints(vec![
                Constraint::Percentage(self.split_ratio),
                Constraint::Percentage(100 - self.split_ratio as u16),
            ])
            .split(area);

        let left_block = Block::default()
            .title_top(
                Line::from(" Termbooru ")
                    .bold()
                    .alignment(Alignment::Center),
            )
            .title_bottom(
                Line::from(vec![
                    " Decrement ".into(),
                    "<Left>".blue().bold(),
                    " Increment ".into(),
                    "<Right>".blue().bold(),
                    " Quit ".into(),
                    "<Q> ".blue().bold(),
                ])
                .alignment(Alignment::Center),
            )
            .borders(Borders::ALL)
            .border_set(border::THICK);

        let mut counter_text_vec = Vec::new();

        counter_text_vec.push(Line::from(vec!["Information".bold().into()]));

        counter_text_vec.push(Line::from(vec![
            "ID: ".into(),
            self.post.id.to_string().into(),
        ]));

        counter_text_vec.push(Line::from(vec![
            "Uploader: ".into(),
            self.post.uploader_id.to_string().into(), // TODO(samuelsung): uploader name
        ]));

        counter_text_vec.push(Line::from(vec![
            "Date: ".into(),
            self.post.created_at.to_string().into(), // TODO(samuelsung): + relative date
        ]));

        counter_text_vec.push(Line::from(vec![
            "Size: ".into(),
            self.post.file_size.to_string().into(), // TODO(samuelsung): human readable
            format!(" .{}", self.post.file_ext).into(),
            format!(" ({}x{})", self.post.image_width, self.post.image_height).into(),
        ]));

        counter_text_vec.push(Line::from(vec![
            "Source: ".into(),
            self.post.source.clone().into(),
        ]));

        counter_text_vec.push(Line::from(vec![
            "Rating: ".into(),
            self.post
                .rating
                .as_ref()
                .map(|r| r.to_string())
                .unwrap_or("-".to_string())
                .into(),
        ]));

        counter_text_vec.push(Line::from(vec![
            "Score: ".into(),
            self.post.score.to_string().into(),
        ]));

        counter_text_vec.push(Line::from(vec![
            "Favorites: ".into(),
            self.post.fav_count.to_string().into(),
        ]));

        counter_text_vec.push(Line::from(vec![
            "Status: ".into(),
            self.post.media_asset.status.to_string().into(),
        ]));

        counter_text_vec.push(Line::from(vec![]));

        counter_text_vec.push(Line::from(vec!["Options".bold().into()]));

        let favorite_option = if self.is_favorited {
            "Unfavorite (press shift+f)"
        } else {
            "Favorite (press f)"
        };

        counter_text_vec.push(Line::from(vec![favorite_option.into()]));

        counter_text_vec.push(Line::from(vec![]));

        counter_text_vec.push(Line::from(vec!["Artist".bold().into()]));

        self.post.tag_string_artist.iter().for_each(|l| {
            counter_text_vec.push(format!("? {l}").red().into());
        });

        counter_text_vec.push(Line::from(vec![]));

        counter_text_vec.push(Line::from(vec!["Copyrights".bold().into()]));

        self.post.tag_string_copyright.iter().take(3).for_each(|l| {
            counter_text_vec.push(format!("? {l}").cyan().into());
        });

        let copyright_len = self.post.tag_string_copyright.len();

        if copyright_len > 3 {
            counter_text_vec.push(
                format!("(and {} tags more...)", copyright_len - 3)
                    .cyan()
                    .into(),
            );
        }

        counter_text_vec.push(Line::from(vec![]));

        counter_text_vec.push(Line::from(vec!["Character".bold().into()]));

        self.post.tag_string_character.iter().take(3).for_each(|l| {
            counter_text_vec.push(format!("? {l}").green().into());
        });

        let character_len = self.post.tag_string_character.len();

        if character_len > 3 {
            counter_text_vec.push(
                format!("(and {} tags more...)", character_len - 3)
                    .green()
                    .into(),
            );
        }

        counter_text_vec.push(Line::from(vec![]));

        counter_text_vec.push(Line::from(vec!["General".bold().into()]));

        self.post.tag_string_general.iter().take(3).for_each(|l| {
            counter_text_vec.push(format!("? {l}").blue().into());
        });

        let general_len = self.post.tag_string_general.len();

        if general_len > 3 {
            counter_text_vec.push(
                format!("(and {} tags more...)", general_len - 3)
                    .blue()
                    .into(),
            );
        }

        counter_text_vec.push(Line::from(vec![]));

        counter_text_vec.push(Line::from(vec!["Meta".bold().into()]));

        self.post.tag_string_meta.iter().take(3).for_each(|l| {
            counter_text_vec.push(format!("? {l}").yellow().into());
        });

        let meta_len = self.post.tag_string_meta.len();

        if meta_len > 3 {
            counter_text_vec.push(
                format!("(and {} tags more...)", meta_len - 3)
                    .yellow()
                    .into(),
            );
        }

        let counter_text = Text::from(counter_text_vec);

        let paragraph = Paragraph::new(counter_text.clone()).wrap(Wrap { trim: true });

        frame.render_widget(paragraph, left_block.inner(layout[0]));

        frame.render_widget(left_block, layout[0]);

        let right_block = Block::default()
            .title(Line::from(" Image ".bold()).alignment(Alignment::Center))
            .borders(Borders::ALL)
            .border_set(border::THICK);

        if let Some(image_protocol) = &mut self.image_protocol {
            let image = AsyncStatefulImage::new();
            frame.render_stateful_widget(image, right_block.inner(layout[1]), image_protocol);
        }

        frame.render_widget(right_block, layout[1]);

        tracing::trace!("done rendering post");
    }
}
pub struct ChangeSplitRatioInDiff(pub i16);

impl SubHandler<ChangeSplitRatioInDiff> for PostState {
    type Return = ();

    async fn handle(
        &mut self,
        ChangeSplitRatioInDiff(diff): ChangeSplitRatioInDiff,
        ctx: &Ctx,
    ) -> Self::Return {
        let next_ratio: i16 = (self.split_ratio as i16) + diff;
        let current_ratio = self.split_ratio;

        if next_ratio > 100 {
            self.split_ratio = 100;
        } else if next_ratio < 0 {
            self.split_ratio = 0
        } else {
            self.split_ratio = next_ratio as u16;
        }

        tracing::debug!(
            "split_ratio: {}, current_ratio: {}",
            self.split_ratio,
            current_ratio
        );

        if self.split_ratio != current_ratio {
            ctx.render_manager.request().await;
        }
    }
}

pub struct LoadImage(pub AsyncStatefulProtocol);

impl SubHandler<LoadImage> for PostState {
    type Return = ();

    async fn handle(&mut self, LoadImage(image_protocol): LoadImage, ctx: &Ctx) {
        self.image_protocol = Some(image_protocol);
        ctx.render_manager.request().await;
    }
}

pub struct OnLoginChangedReq(pub Option<usize>);

impl SubHandler<OnLoginChangedReq> for PostState {
    type Return = ();

    async fn handle(
        &mut self,
        OnLoginChangedReq(user_id): OnLoginChangedReq,
        ctx: &Ctx,
    ) -> Self::Return {
        let post_id = self.post.id;
        let page_id = self.page_id;
        let remote_manager_address = ctx.remote_manager_address.clone();
        let tui_address = ctx.tui_address.clone();

        tokio::spawn(async move {
            let is_favorited = if let Some(user_id) = user_id {
                remote_manager_address
                    .send(Request::from(
                        get_favorites()
                            .user_id(NumberField::Eq(user_id))
                            .post_id(NumberField::Eq(post_id)),
                    ))
                    .await
                    .expect("Request manager should not be dropped")
                    .await
                    .expect("Tokio spawn panic")
                    .ok()
                    .map(|v| !v.is_empty())
                    .unwrap_or(false)
            } else {
                false
            };

            tui_address
                .send(PageMessage::post(
                    page_id,
                    OnLoginChangedRes { is_favorited },
                ))
                .detach()
                .await
        });
    }
}

pub struct OnLoginChangedRes {
    pub is_favorited: bool,
}

impl SubHandler<OnLoginChangedRes> for PostState {
    type Return = ();

    async fn handle(
        &mut self,
        OnLoginChangedRes { is_favorited }: OnLoginChangedRes,
        ctx: &Ctx,
    ) -> Self::Return {
        let last_is_favorited = self.is_favorited;
        self.is_favorited = is_favorited;

        if last_is_favorited != is_favorited {
            ctx.render_manager.request().await;
        }
    }
}
