use std::mem;

use crossterm::event::{KeyCode, KeyModifiers};
use ratatui::{
    layout::Rect,
    style::{Color, Stylize},
    text::{Line, Text},
    Frame,
};
use unicode_width::UnicodeWidthStr;

use crate::{
    autocompletes::{self, AutoCompleteReqMode, AutoCompleteSuggestion},
    key_press::KeyPress,
    tui::{commands, posts_actions, Ctx, SubHandler, Tui},
};

#[derive(Debug)]
pub enum PromptMode {
    Idle,
    Input {
        header: String,
        text: String,
        autocompletes: Vec<AutoCompleteSuggestion>,
        autocomplete_picked: Option<usize>,
        target_autocompletes_version: u32,
        current_autocompletes_version: u32,
        cursor_string_pos: usize,
    },
}

impl Default for PromptMode {
    fn default() -> Self {
        Self::Idle
    }
}

#[derive(Debug, Default)]
pub struct Prompt {
    mode: PromptMode,
    idle_display: String,
}

impl Prompt {
    pub fn can_handle_key_event(&self) -> bool {
        matches!(self.mode, PromptMode::Input { .. })
    }

    pub fn render(&mut self, frame: &mut Frame, area: Rect) {
        tracing::debug!("start render prompt");

        match &self.mode {
            PromptMode::Idle => {
                let text = Text::from(self.idle_display.clone());
                frame.render_widget(text, area);
            }
            PromptMode::Input {
                header,
                text,
                autocomplete_picked: Some(autocomplete_picked),
                autocompletes,
                cursor_string_pos,
                ..
            } if autocompletes.get(*autocomplete_picked).is_some() => {
                let autocomplete = &autocompletes[*autocomplete_picked].text;

                let (start, end) = text.split_at(*cursor_string_pos);
                let start = start.rsplit_once(' ').map(|(t, _)| t);

                let (next_text, next_cursor_string_pos) = start
                    .map(|start| {
                        (
                            format!("{start} {}{end}", autocomplete),
                            start.len() + 1 + autocomplete.len(),
                        )
                    })
                    .unwrap_or_else(|| (format!("{}{end}", autocomplete), autocomplete.len()));

                frame.set_cursor_position((
                    (header.width() + next_text[..next_cursor_string_pos].width())
                        .try_into()
                        .unwrap_or(0),
                    area.y,
                ));

                frame.render_widget(Line::from(vec![header.into(), next_text.into()]), area);
            }
            PromptMode::Input {
                header,
                text,
                cursor_string_pos,
                ..
            } => {
                frame.render_widget(Text::from(format!("{header}{text}")), area);
                frame.set_cursor_position((
                    (header.width() + text[..*cursor_string_pos].width())
                        .try_into()
                        .unwrap_or(0),
                    area.y,
                ));
            }
        }

        tracing::info!("end render prompt");
    }

    pub fn render_autocomplete(&self, frame: &mut Frame, area: Rect) {
        if let PromptMode::Input {
            autocompletes,
            autocomplete_picked,
            ..
        } = &self.mode
        {
            let list = autocompletes.iter().enumerate().fold(
                Vec::new(),
                |mut acc, (idx, autocomplete)| {
                    let bg_color = if autocomplete_picked.is_some_and(|i| i == idx) {
                        Color::Blue
                    } else {
                        Color::Black
                    };
                    if idx != 0 {
                        acc.push(" ".into());
                    }
                    acc.append(
                        &mut autocomplete
                            .display
                            .iter()
                            .map(|span| span.clone().bg(bg_color))
                            .collect(),
                    );
                    acc
                },
            );

            let text = Line::from(list);

            frame.render_widget(text, area);
        }
    }
}

impl<M> xtra::Handler<M> for Tui
where
    Prompt: SubHandler<M>,
{
    type Return = <Prompt as SubHandler<M>>::Return;

    fn handle(
        &mut self,
        message: M,
        _ctx: &mut xtra::Context<Self>,
    ) -> impl std::future::Future<Output = Self::Return> + Send {
        self.state.prompt.handle(message, &self.ctx)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct ApplyAutoCompleteSuggestion;

impl SubHandler<ApplyAutoCompleteSuggestion> for Prompt {
    type Return = ();

    #[tracing::instrument(skip(self, ctx))]
    async fn handle(&mut self, _message: ApplyAutoCompleteSuggestion, ctx: &Ctx) -> Self::Return {
        tracing::debug!("entry");
        match &mut self.mode {
            PromptMode::Input {
                text,
                cursor_string_pos,
                autocompletes,
                autocomplete_picked,
                ..
            } if autocomplete_picked.is_some_and(|idx| autocompletes.get(idx).is_some()) => {
                let autocomplete_picked = autocomplete_picked.take().unwrap();
                let autocomplete = &autocompletes[autocomplete_picked].text;

                let (start, end) = text.split_at(*cursor_string_pos);
                let start = start.rsplit_once(' ').map(|(t, _)| t);

                let (mut next_text, next_cursor_string_pos) = start
                    .map(|start| {
                        (
                            format!("{start} {}{end}", autocomplete),
                            start.len() + 1 + autocomplete.len(),
                        )
                    })
                    .unwrap_or_else(|| (format!("{}{end}", autocomplete), autocomplete.len()));

                *cursor_string_pos = next_cursor_string_pos;
                mem::swap(text, &mut next_text);

                ctx.render_manager.request().await;
            }
            _ => {}
        }
    }
}

pub struct AutoCompleteReq;

impl SubHandler<AutoCompleteReq> for Prompt {
    type Return = ();

    async fn handle(&mut self, _message: AutoCompleteReq, ctx: &Ctx) -> Self::Return {
        if let PromptMode::Input {
            header,
            text,
            target_autocompletes_version,
            cursor_string_pos,
            ..
        } = &mut self.mode
        {
            let req = autocompletes::AutoCompleteReq {
                tokens: text[..*cursor_string_pos]
                    .split_whitespace()
                    .map(|str| str.to_string())
                    .collect(),
                mode: if text[..*cursor_string_pos]
                    .chars()
                    .next_back()
                    .is_some_and(|c| !c.is_whitespace())
                {
                    AutoCompleteReqMode::AppendLast
                } else {
                    AutoCompleteReqMode::New
                },
            };
            *target_autocompletes_version += 1;
            let query_version = *target_autocompletes_version;

            let is_tag = "/" == header;

            let ctx = ctx.clone();
            tokio::spawn(async move {
                let res = if is_tag {
                    req.resolve_tag(&ctx).await
                } else {
                    req.resolve(&ctx).await
                };

                match res {
                    Ok(res) => {
                        let _ = ctx
                            .tui_address
                            .send(AutoCompleteRes(query_version, res))
                            .detach()
                            .await;
                    }
                    Err(err) => {
                        let _ = ctx
                            .tui_address
                            .send(AutoCompleteErr(query_version, err))
                            .detach()
                            .await;
                    }
                }
            });
        }
    }
}

pub struct AutoCompleteRes(pub u32, pub autocompletes::AutoCompleteRes);

impl SubHandler<AutoCompleteRes> for Prompt {
    type Return = ();

    async fn handle(
        &mut self,
        AutoCompleteRes(query_version, res): AutoCompleteRes,
        ctx: &Ctx,
    ) -> Self::Return {
        if let PromptMode::Input {
            autocompletes,
            target_autocompletes_version,
            current_autocompletes_version,
            ..
        } = &mut self.mode
        {
            if *target_autocompletes_version != *current_autocompletes_version {
                *autocompletes = res;
                *current_autocompletes_version = query_version;
            }

            ctx.render_manager.request().await;
        }
    }
}

pub struct AutoCompleteErr(pub u32, pub autocompletes::AutoCompleteErr);

impl SubHandler<AutoCompleteErr> for Prompt {
    type Return = ();

    async fn handle(
        &mut self,
        AutoCompleteErr(query_version, err): AutoCompleteErr,
        ctx: &Ctx,
    ) -> Self::Return {
        if let PromptMode::Input {
            autocompletes,
            target_autocompletes_version,
            current_autocompletes_version,
            ..
        } = &mut self.mode
        {
            if *target_autocompletes_version != *current_autocompletes_version {
                *autocompletes = Vec::new();
                *current_autocompletes_version = query_version;
            }

            ctx.render_manager.request().await;
        }

        if !matches!(err, autocompletes::AutoCompleteErr::Unsupported) {
            let _ = ctx
                .tui_address
                .send(Echo(format!("[Error] {err}")))
                .detach()
                .await;
        }
    }
}

pub struct CancelInput;

impl SubHandler<CancelInput> for Prompt {
    type Return = ();

    async fn handle(&mut self, _message: CancelInput, ctx: &Ctx) -> Self::Return {
        self.mode = PromptMode::Idle;
        ctx.render_manager.request().await;
    }
}

pub struct Echo(pub String);

impl SubHandler<Echo> for Prompt {
    type Return = ();

    async fn handle(&mut self, Echo(text): Echo, ctx: &Ctx) -> Self::Return {
        self.idle_display = text;
        ctx.render_manager.request().await;
    }
}

pub struct PickAutoCompleteInDiff(pub isize);

impl SubHandler<PickAutoCompleteInDiff> for Prompt {
    type Return = ();

    async fn handle(
        &mut self,
        PickAutoCompleteInDiff(idx_diff): PickAutoCompleteInDiff,
        ctx: &Ctx,
    ) -> Self::Return {
        if let PromptMode::Input {
            autocompletes,
            autocomplete_picked,
            ..
        } = &self.mode
        {
            if !autocompletes.is_empty() {
                self.handle(
                    PickAutoComplete(
                        autocomplete_picked
                            .as_ref()
                            .map(|i| *i as isize + idx_diff)
                            .unwrap_or(0)
                            .rem_euclid(autocompletes.len() as isize)
                            as usize,
                    ),
                    ctx,
                )
                .await;
            }
        }
    }
}

pub struct PickAutoComplete(pub usize);

impl SubHandler<PickAutoComplete> for Prompt {
    type Return = ();

    async fn handle(&mut self, PickAutoComplete(idx): PickAutoComplete, ctx: &Ctx) -> Self::Return {
        if let PromptMode::Input {
            autocomplete_picked,
            ..
        } = &mut self.mode
        {
            *autocomplete_picked = Some(idx);
            ctx.render_manager.request().await;
        }
    }
}

pub struct StartInput {
    pub header: String,
}

impl SubHandler<StartInput> for Prompt {
    type Return = ();

    async fn handle(&mut self, StartInput { header }: StartInput, ctx: &Ctx) -> Self::Return {
        self.mode = PromptMode::Input {
            header,
            autocompletes: Vec::new(),
            autocomplete_picked: None,
            current_autocompletes_version: 0,
            target_autocompletes_version: 0,
            text: String::new(),
            cursor_string_pos: 0,
        };

        self.handle(AutoCompleteReq, ctx).await;

        ctx.render_manager.request().await;
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SubmitOutput;

impl SubHandler<SubmitOutput> for Prompt {
    type Return = ();

    #[tracing::instrument(skip(self, ctx))]
    async fn handle(&mut self, _message: SubmitOutput, ctx: &Ctx) -> Self::Return {
        tracing::debug!("entry");
        if self.can_handle_key_event() {
            if let PromptMode::Input { header, text, .. } = mem::take(&mut self.mode) {
                match header.as_str() {
                    ":" => {
                        let _ = ctx
                            .tui_address
                            .send(commands::RawCommand(text))
                            .detach()
                            .await;
                    }
                    "/" => {
                        let _ = ctx
                            .tui_address
                            .send(posts_actions::ToPosts { tags: vec![text] })
                            .detach()
                            .await;
                    }
                    _ => {
                        let _ = ctx
                            .tui_address
                            .send(Echo(format!("[Error] unknown command header {header}")))
                            .detach()
                            .await;
                    }
                };
            }

            ctx.render_manager.request().await;
        }
    }
}

#[derive(Debug, Clone)]
pub struct PromptKeyPress(pub KeyPress);

impl SubHandler<PromptKeyPress> for Prompt {
    type Return = ();

    #[tracing::instrument(skip(self, ctx))]
    async fn handle(
        &mut self,
        PromptKeyPress(key_press): PromptKeyPress,
        ctx: &Ctx,
    ) -> Self::Return {
        tracing::debug!("entry");
        match (key_press, &mut self.mode) {
            (
                key_press @ (KeyPress(KeyModifiers::NONE | KeyModifiers::SHIFT, KeyCode::Char(_))
                | KeyPress(KeyModifiers::NONE, KeyCode::Backspace)
                | KeyPress(KeyModifiers::NONE, KeyCode::Delete)),
                PromptMode::Input {
                    autocomplete_picked: Some(_),
                    ..
                },
            ) => {
                self.handle(ApplyAutoCompleteSuggestion, ctx).await;
                let _ = ctx.tui_address.send(key_press).priority(0).detach().await;
            }

            (
                KeyPress(KeyModifiers::NONE, KeyCode::Backspace),
                PromptMode::Input {
                    text,
                    cursor_string_pos,
                    ..
                },
            ) => {
                let (start, end) = text.split_at(*cursor_string_pos);
                let mut start_chars = start.chars();
                let popped = start_chars.next_back();

                if let Some(popped) = popped {
                    *cursor_string_pos -= popped.len_utf8();
                    *text = format!("{}{end}", start_chars.as_str());

                    self.handle(AutoCompleteReq, ctx).await;

                    ctx.render_manager.request().await;
                } else {
                    self.handle(CancelInput, ctx).await;
                }
            }

            (
                KeyPress(KeyModifiers::NONE, KeyCode::Enter),
                PromptMode::Input {
                    autocomplete_picked: Some(_),
                    ..
                },
            ) => {
                self.handle(ApplyAutoCompleteSuggestion, ctx).await;
            }

            (KeyPress(KeyModifiers::NONE, KeyCode::Enter), PromptMode::Input { .. }) => {
                self.handle(SubmitOutput, ctx).await;
            }

            (
                KeyPress(KeyModifiers::NONE, KeyCode::Left),
                PromptMode::Input {
                    autocomplete_picked: Some(_),
                    ..
                },
            ) => {
                self.handle(PickAutoCompleteInDiff(-1), ctx).await;
            }

            (
                KeyPress(KeyModifiers::NONE, KeyCode::Left),
                PromptMode::Input {
                    text,
                    cursor_string_pos,
                    ..
                },
            ) => {
                if let Some(char) = text[..*cursor_string_pos].chars().next_back() {
                    *cursor_string_pos -= char.len_utf8();

                    self.handle(AutoCompleteReq, ctx).await;
                    ctx.render_manager.request().await;
                }
            }

            (
                KeyPress(KeyModifiers::NONE, KeyCode::Right),
                PromptMode::Input {
                    autocomplete_picked: Some(_),
                    ..
                },
            ) => {
                self.handle(PickAutoCompleteInDiff(1), ctx).await;
            }

            (
                KeyPress(KeyModifiers::NONE, KeyCode::Right),
                PromptMode::Input {
                    text,
                    cursor_string_pos,
                    ..
                },
            ) => {
                if let Some(char) = text[*cursor_string_pos..].chars().next() {
                    *cursor_string_pos += char.len_utf8();

                    self.handle(AutoCompleteReq, ctx).await;
                    ctx.render_manager.request().await;
                }
            }

            (KeyPress(KeyModifiers::NONE, KeyCode::Tab), PromptMode::Input { .. }) => {
                self.handle(PickAutoCompleteInDiff(1), ctx).await;
            }

            (
                KeyPress(KeyModifiers::SHIFT, KeyCode::Tab | KeyCode::BackTab),
                PromptMode::Input { .. },
            ) => {
                self.handle(PickAutoCompleteInDiff(-1), ctx).await;
            }

            (
                KeyPress(KeyModifiers::NONE, KeyCode::Delete),
                PromptMode::Input {
                    text,
                    cursor_string_pos,
                    ..
                },
            ) => {
                let (start, end) = text.split_at(*cursor_string_pos);
                let mut end_chars = end.chars();
                let popped = end_chars.next();

                if popped.is_some() {
                    *text = format!("{start}{}", end_chars.as_str());
                    self.handle(AutoCompleteReq, ctx).await;
                    ctx.render_manager.request().await;
                } else {
                    self.handle(CancelInput, ctx).await;
                }
            }
            (
                KeyPress(KeyModifiers::NONE | KeyModifiers::SHIFT, KeyCode::Char(char)),
                PromptMode::Input {
                    text,
                    cursor_string_pos,
                    ..
                },
            ) => {
                let (start, end) = text.split_at(*cursor_string_pos);

                *text = format!("{start}{char}{end}");
                *cursor_string_pos += char.len_utf8();

                self.handle(AutoCompleteReq, ctx).await;
                ctx.render_manager.request().await;
            }

            (KeyPress(KeyModifiers::NONE, KeyCode::Esc), PromptMode::Input { .. }) => {
                self.handle(CancelInput, ctx).await;
            }

            _ => {}
        }
    }
}
