use std::{
    cmp::{max, min},
    io, mem,
    sync::Arc,
};

use derive_debug::Dbg;
use image::Rgba;
use ratatui::{
    prelude::*,
    symbols::border,
    widgets::{block::*, *},
};
use ratatui_image::{FilterType, Resize};
use rdanbooru::{
    apis::{get_file, get_posts},
    models::{media_asset::VariantType, post::Post},
    ClientError,
};
use tokio::task::{JoinHandle, JoinSet};
use url::Url;

use crate::{
    remotes::Request,
    tui::{
        image::{AsyncStatefulImage, AsyncStatefulProtocol},
        motions,
        pages::{PageId, PageMessage},
        post_actions, Ctx, OnResize, SubHandler,
    },
};

use super::{post::PostStateBuilder, prompt};

pub fn preview_url_of_post(post: &Post) -> Option<&Url> {
    post.media_asset
        .variants
        .iter()
        .find(|v| matches!(v.variant_type, VariantType::Thumbnail360x360))
        .map(|v| &v.url)
}

#[derive(Debug, thiserror::Error)]
pub enum PostsPageBuildError {
    #[error("failed to read/write")]
    IO(#[from] io::Error),

    #[error("error during fetch post: {0}")]
    FetchPost(#[from] ClientError),

    #[error("failed to read image: {0}")]
    ReadImage(#[from] image::ImageError),
}

#[derive(Debug, Default)]
pub struct PostsPageBuilder {
    tags: Option<Vec<String>>, // TODO(samuelsung): not cloning everytime
    page_no: Option<usize>,
    preview_image_rect: Option<(u16, u16)>,
    size: Option<usize>,
}

impl PostsPageBuilder {
    pub fn tags(mut self, value: Vec<String>) -> Self {
        self.tags = Some(value);
        self
    }

    pub fn page_no(mut self, value: usize) -> Self {
        self.page_no = Some(value);
        self
    }

    pub fn preview_image_rect(mut self, value: (u16, u16)) -> Self {
        self.preview_image_rect = Some(value);
        self
    }

    pub fn size(mut self, value: usize) -> Self {
        self.size = Some(value);
        self
    }

    #[tracing::instrument(skip(ctx))]
    pub async fn build(self, ctx: &Ctx) -> Result<PostsPage, PostsPageBuildError> {
        let tags = self.tags.unwrap_or_default();
        let size = self.size.unwrap_or(20);
        let page = self.page_no.unwrap_or_default();

        let image_picker = ctx.image_picker.clone();
        let remote_manager_addr = ctx.remote_manager_address.clone();
        let render_manager = ctx.render_manager.clone();

        tracing::debug!("start fetch posts");

        let posts = remote_manager_addr
            .send(Request::new(get_posts().tags(tags).page(page).limit(size)))
            .await
            .expect("Request manager should not be dropped")
            .await
            .expect("Tokio spawn panic")?;

        tracing::debug!("done fetch posts");

        let mut join_set = JoinSet::new();

        for (post_id, url) in posts
            .iter()
            .flat_map(|post| preview_url_of_post(post).cloned().map(|url| (post.id, url)))
        {
            let remote_manager_addr = remote_manager_addr.clone();
            let render_manager = render_manager.clone();
            let image_picker = image_picker.clone();

            join_set.spawn(async move {
                tracing::debug!("start fetch posts' image");

                let bytes = remote_manager_addr
                    .send(Request::new(get_file(url)))
                    .await
                    .expect("Request manager should not be dropped")
                    .await
                    .expect("Tokio spawn panic")?;

                tracing::debug!("done fetch posts' image");

                let dynamic_image = image::ImageReader::new(io::Cursor::new(bytes.as_ref()))
                    .with_guessed_format()
                    .map_err(PostsPageBuildError::from)
                    .and_then(|f| Ok(f.decode()?))?;

                let mut protocol = AsyncStatefulProtocol::new(
                    render_manager,
                    image_picker.new_resize_protocol(dynamic_image),
                );

                tracing::debug!("done protocl");

                if let Some((width, height)) = self.preview_image_rect {
                    let _ = protocol
                        .try_resize(
                            Resize::Fit(Some(FilterType::Lanczos3)),
                            Rgba([0, 0, 0, 0]),
                            Rect {
                                width,
                                height,
                                ..Default::default()
                            },
                        )
                        .await;
                }

                tracing::debug!("done resize");

                Ok::<_, PostsPageBuildError>((post_id, protocol))
            });
        }

        let mut posts: Vec<(Arc<Post>, Option<AsyncStatefulProtocol>)> =
            posts.into_iter().map(|post| (post.into(), None)).collect();

        while let Some(res) = join_set.join_next().await {
            match res {
                Ok(Ok((post_id, protocol))) => {
                    if let Some(pro) = posts
                        .iter_mut()
                        .find(|(p, _)| p.id == post_id)
                        .map(|(_, pro)| pro)
                    {
                        *pro = Some(protocol);
                    }
                }
                _ => {}
            }
        }

        tracing::debug!("done fetch posts' image");

        Ok(PostsPage {
            posts,
            page_no: self.page_no.unwrap_or_default(),
        })
    }
}

#[derive(Dbg)]
pub struct PostsPage {
    #[dbg(placeholder = "...")]
    posts: Vec<(Arc<Post>, Option<AsyncStatefulProtocol>)>,

    page_no: usize,
}

impl PostsPage {
    pub fn update_post(&mut self, post: &Arc<Post>) -> bool {
        let post_id = post.id;

        if let Some((ref mut p, _)) = self.posts.iter_mut().find(|(p, _)| p.id == post_id) {
            *p = post.clone();
            return true;
        }

        false
    }
}

#[derive(Dbg)]
pub enum PostsQueryState {
    Idle(usize),
    Loading(usize, #[dbg(skip)] JoinHandle<()>),
}

#[derive(Dbg)]
pub struct PostsState {
    pub page_id: PageId,
    pub tags: Vec<String>,
    pub page: PostsPage,
    pub query_state: PostsQueryState,
    pub query_version: usize,

    pub picked_in_frame: (u16, u16),
    pub preview_image_rect: Option<(u16, u16)>,
    pub size: (u16, u16),
}

impl PostsState {
    pub async fn new(
        page_id: PageId,
        tags: Vec<String>,
        size: (u16, u16),
        ctx: &Ctx,
    ) -> Result<Self, PostsPageBuildError> {
        let size = Self::posts_size(size.0, size.1);
        let limit = (size.0 * size.1) as usize;

        let page = PostsPageBuilder::default()
            .size(limit)
            .tags(tags.clone())
            .page_no(1)
            .build(ctx)
            .await?;

        Ok(PostsState {
            page_id,
            tags: tags.clone(),
            query_state: PostsQueryState::Idle(1),
            page,
            query_version: 0,

            picked_in_frame: (0, 0),
            preview_image_rect: None,
            size,
        })
    }

    pub fn posts_size(width: u16, height: u16) -> (u16, u16) {
        (min(max(width / 20, 1), 8), min(max(height / 10, 1), 4))
    }

    pub fn page_info(&self) -> String {
        format!(
            "posts: /{} [{}:{}]",
            self.tags.join(" "),
            self.picked_idx() + 1,
            match self.query_state {
                PostsQueryState::Idle(page_no) => format!("{page_no}"),
                PostsQueryState::Loading(page_no, _) if self.page.page_no > page_no => {
                    format!("({page_no} <- {})", self.page.page_no)
                }
                PostsQueryState::Loading(page_no, _) if self.page.page_no < page_no => {
                    format!("({} -> {page_no})", self.page.page_no)
                }
                PostsQueryState::Loading(page_no, _) => {
                    format!("{page_no} (refreshing...)")
                }
            }
        )
    }

    pub fn picked_idx(&self) -> usize {
        let (x, y) = self.picked_in_frame;
        let (col, _row) = self.size;

        (y * col + x).into()
    }

    pub fn picked_post(&self) -> Option<&Post> {
        self.picked_post_with_preview().map(|(post, _)| post)
    }

    pub fn picked_post_with_preview(&self) -> Option<(&Post, Option<&AsyncStatefulProtocol>)> {
        self.page
            .posts
            .get(self.picked_idx())
            .map(|(post, preview)| (post.as_ref(), preview.as_ref()))
    }

    #[tracing::instrument(skip(frame, area))]
    pub fn render(&mut self, frame: &mut Frame, area: Rect) {
        tracing::trace!("start rendering posts");
        let (col, row) = self.size;
        let (col_step, row_step) = (100 / col, 100 / row);

        let row_constraints: Vec<_> = (0..row)
            .rev()
            .into_iter()
            .map(|step| Constraint::Percentage(100 - step * row_step))
            .collect();

        let col_constraints: Vec<_> = (0..col)
            .rev()
            .into_iter()
            .map(|step| Constraint::Percentage(100 - step * col_step))
            .collect();

        let row_layout = Layout::default()
            .direction(Direction::Vertical)
            .constraints(row_constraints)
            .split(area);

        let col_layout = Layout::default()
            .direction(Direction::Horizontal)
            .constraints(col_constraints);

        let block = Block::default()
            .borders(Borders::ALL)
            .border_set(border::THICK);

        // NOTE(samuelsung): find the min width/height of all rect to avoid resizing when moving
        let (mut min_width, mut min_height) = (u16::MAX, u16::MAX);
        for i in 0..row {
            let splited_col = col_layout.split(row_layout[i as usize]);

            for j in 0..col {
                let rect: Rect = splited_col[j as usize];
                min_width = min(rect.width, min_width);
                min_height = min(rect.height, min_height);
            }
        }

        let min_rect = Rect {
            width: min_width,
            height: min_height,
            ..Default::default()
        };
        let min_rect_inner = block.inner(min_rect);

        self.preview_image_rect = Some((min_rect_inner.width, min_rect_inner.height));

        for y in 0..row {
            let splited_col = col_layout.split(row_layout[y as usize]);

            for x in 0..col {
                let mut rect: Rect = splited_col[x as usize];
                rect.width = min_width;
                rect.height = min_height;

                let idx = (y * col + x) as usize;

                if self.picked_idx() == idx {
                    let mut block = block.clone();
                    if let Some((post, _)) = self.page.posts.get(idx) {
                        block = block.title(
                            Line::from(format!(" {} ", post.id).bold())
                                .alignment(Alignment::Center),
                        );
                    }

                    frame.render_widget(block, rect.clone());
                }

                if let Some((_post, ref mut protocol)) = self.page.posts.get_mut(idx) {
                    // let paragraph = Paragraph::new(vec![post.id.to_string().into()])
                    //     .wrap(Wrap { trim: true })
                    //     .centered();
                    // frame.render_widget(paragraph, rect);
                    if let Some(protocol) = protocol {
                        let image = AsyncStatefulImage::new();
                        frame.render_stateful_widget(image, block.inner(rect), protocol);
                    }
                }
            }
        }
        tracing::trace!("done rendering posts");
    }
}

pub struct MovePickedAbs(pub isize, pub isize);

impl SubHandler<MovePickedAbs> for PostsState {
    type Return = ();

    async fn handle(
        &mut self,
        MovePickedAbs(next_x, next_y): MovePickedAbs,
        ctx: &Ctx,
    ) -> Self::Return {
        let (current_x, current_y) = self.picked_in_frame;
        let (col, row) = self.size;

        let next_x = max(min(next_x, col as isize - 1), 0) as u16;
        let next_y = max(min(next_y, row as isize - 1), 0) as u16;

        let is_moved = next_x != current_x || next_y != current_y;
        self.picked_in_frame = (next_x, next_y);

        tracing::debug!("current: ({current_x}, {current_y})");
        tracing::debug!("picked_in_frame: ({next_x}, {next_y})");

        if is_moved {
            ctx.render_manager.request().await;
        }
    }
}

pub struct MovePickedRel(pub isize, pub isize);

impl SubHandler<MovePickedRel> for PostsState {
    type Return = ();

    async fn handle(&mut self, MovePickedRel(x, y): MovePickedRel, ctx: &Ctx) -> Self::Return {
        let (current_x, current_y) = self.picked_in_frame;
        let (next_x, next_y) = (current_x as isize + x, current_y as isize + y);

        self.handle(MovePickedAbs(next_x, next_y), ctx).await
    }
}

impl SubHandler<motions::NextPage> for PostsState {
    type Return = ();

    async fn handle(&mut self, _message: motions::NextPage, ctx: &Ctx) -> Self::Return {
        match self.query_state {
            PostsQueryState::Idle(page_no) | PostsQueryState::Loading(page_no, _) => {
                self.handle(PostsReq(page_no + 1), ctx).await;
            }
        }
    }
}

pub struct OnPostUpdated(pub Arc<Post>);

impl SubHandler<OnPostUpdated> for PostsState {
    type Return = ();

    async fn handle(&mut self, OnPostUpdated(post): OnPostUpdated, ctx: &Ctx) -> Self::Return {
        let updated = self.page.update_post(&post);

        if updated {
            ctx.render_manager.request().await;
        }
    }
}

impl SubHandler<OnResize> for PostsState {
    type Return = ();

    async fn handle(&mut self, OnResize(col, row): OnResize, ctx: &Ctx) -> Self::Return {
        let (last_col, last_row) = self.size;
        self.size = Self::posts_size(col, row);
        self.handle(MovePickedRel(0, 0), ctx).await;

        if last_col != self.size.0 || last_row != self.size.1 {
            ctx.render_manager.request().await;
        }
    }
}

pub struct PickImage;

impl SubHandler<PickImage> for PostsState {
    type Return = ();

    async fn handle(&mut self, _message: PickImage, ctx: &Ctx) -> Self::Return {
        if let Some((post, preview)) = self.picked_post_with_preview() {
            let post = post.clone();
            let mut post_state_builder = PostStateBuilder::default()
                .post(post)
                .sync_fetch_image(false);

            if let Some(preview) = preview {
                post_state_builder = post_state_builder.preview_image_protocol(preview.clone())
            }

            let _ = ctx
                .tui_address
                .send(post_actions::ToPostByBuilder(post_state_builder))
                .detach()
                .await;
        }
    }
}

pub struct PostsReq(pub usize);

impl SubHandler<PostsReq> for PostsState {
    type Return = ();

    async fn handle(&mut self, PostsReq(next_page_no): PostsReq, ctx: &Ctx) -> Self::Return {
        if let PostsQueryState::Loading(loading_page_no, loading_handle) = &self.query_state {
            if *loading_page_no == next_page_no {
                return;
            }

            loading_handle.abort();
        }

        let page_id = self.page_id;
        let query_version = self.query_version;
        let size = {
            let (col, row) = self.size;
            col as usize * row as usize
        };

        let mut builder = PostsPageBuilder::default()
            .tags(self.tags.clone())
            .page_no(next_page_no)
            .size(size);

        if let Some(preview_image_rect) = self.preview_image_rect {
            builder = builder.preview_image_rect(preview_image_rect)
        }

        let render_manager_request = ctx.render_manager.request();
        let ctx = ctx.clone();

        let next_handle = tokio::spawn(async move {
            let res = builder.build(&ctx).await;
            match res {
                Ok(posts_page) => {
                    let _ = ctx
                        .tui_address
                        .send(PageMessage::posts(
                            page_id,
                            PostsRes(posts_page, query_version),
                        ))
                        .detach()
                        .await;
                }
                Err(err) => {
                    let _ = ctx
                        .tui_address
                        .send(PageMessage::posts(page_id, PostsErr(err)))
                        .detach()
                        .await;
                }
            }
        });

        self.query_state = PostsQueryState::Loading(next_page_no, next_handle);
        render_manager_request.await;
    }
}

pub struct PostsRes(pub PostsPage, pub usize);

impl SubHandler<PostsRes> for PostsState {
    type Return = ();

    async fn handle(
        &mut self,
        PostsRes(mut posts_page, query_version): PostsRes,
        ctx: &Ctx,
    ) -> Self::Return {
        if self.query_version == query_version {
            self.query_state = PostsQueryState::Idle(posts_page.page_no);
            mem::swap(&mut self.page, &mut posts_page);

            ctx.render_manager.request().await;
        }
    }
}

pub struct PostsErr(pub PostsPageBuildError);

impl SubHandler<PostsErr> for PostsState {
    type Return = ();

    async fn handle(&mut self, PostsErr(err): PostsErr, ctx: &Ctx) -> Self::Return {
        let _ = ctx
            .tui_address
            .send(prompt::Echo(format!("[Error] failed to fetch page: {err}")))
            .detach()
            .await;
    }
}

impl SubHandler<motions::PrevPage> for PostsState {
    type Return = ();

    async fn handle(&mut self, _message: motions::PrevPage, ctx: &Ctx) -> Self::Return {
        match self.query_state {
            PostsQueryState::Idle(page_no) | PostsQueryState::Loading(page_no, _)
                if page_no <= 1 =>
            {
                let _ = ctx
                    .tui_address
                    .send(prompt::Echo(format!(
                        "[Warning] already at the start: page {page_no}",
                    )))
                    .detach()
                    .await;
            }

            PostsQueryState::Idle(page_no) | PostsQueryState::Loading(page_no, _) => {
                self.handle(PostsReq(page_no - 1), ctx).await;
            }
        }
    }
}

impl SubHandler<motions::Refresh> for PostsState {
    type Return = ();

    async fn handle(&mut self, _message: motions::Refresh, ctx: &Ctx) -> Self::Return {
        self.query_version += 1;

        match self.query_state {
            PostsQueryState::Idle(page_no) | PostsQueryState::Loading(page_no, _) => {
                self.handle(PostsReq(page_no), ctx).await;
            }
        }

        ctx.render_manager.request().await;
    }
}

pub struct UpdateTags(pub Vec<String>);

impl SubHandler<UpdateTags> for PostsState {
    type Return = ();

    async fn handle(&mut self, UpdateTags(tags): UpdateTags, ctx: &Ctx) -> Self::Return {
        self.tags = tags;
        self.handle(motions::Refresh, ctx).await;
        ctx.render_manager.request().await;
    }
}
