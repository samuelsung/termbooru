use std::time::Duration;

use crossterm::event::{KeyCode, KeyModifiers};
use enum_as_inner::EnumAsInner;
use xtra::Handler;

use crate::{
    key_press::KeyPress,
    pages::{post, posts, prompt},
    tui::{motions, post_actions, posts_actions, Tui},
};

use super::{pages::Page, SubHandler};

pub struct Confirm;

pub struct Exit;

impl xtra::Handler<Confirm> for Tui {
    type Return = ();

    async fn handle(&mut self, _message: Confirm, _ctx: &mut xtra::Context<Self>) -> Self::Return {
        match self.state.pages.last_mut() {
            Some(Page::Posts(posts_state)) => {
                posts_state.handle(posts::PickImage, &self.ctx).await;
            }
            _ => {}
        }
    }
}

impl xtra::Handler<Exit> for Tui {
    type Return = ();

    async fn handle(&mut self, _message: Exit, ctx: &mut xtra::Context<Self>) -> Self::Return {
        self.state.pages.pop();

        if self.state.pages.is_empty() {
            ctx.stop_self();
        }

        self.ctx.render_manager.request().await;
    }
}

pub struct Up;

pub struct Down;

pub struct Left;

pub struct Right;

impl xtra::Handler<Up> for Tui {
    type Return = ();

    async fn handle(&mut self, _message: Up, _ctx: &mut xtra::Context<Self>) -> Self::Return {
        match self.state.pages.last_mut() {
            Some(Page::Posts(posts_state)) => {
                posts_state
                    .handle(posts::MovePickedRel(0, -1), &self.ctx)
                    .await;
            }
            Some(Page::Post(post_state)) => {
                post_state
                    .handle(post::ChangeSplitRatioInDiff(-1), &self.ctx)
                    .await;
            }
            _ => {}
        }
    }
}

impl xtra::Handler<Down> for Tui {
    type Return = ();

    async fn handle(&mut self, _message: Down, _ctx: &mut xtra::Context<Self>) -> Self::Return {
        match self.state.pages.last_mut() {
            Some(Page::Posts(posts_state)) => {
                posts_state
                    .handle(posts::MovePickedRel(0, 1), &self.ctx)
                    .await;
            }
            Some(Page::Post(post_state)) => {
                post_state
                    .handle(post::ChangeSplitRatioInDiff(1), &self.ctx)
                    .await;
            }
            _ => {}
        }
    }
}

impl xtra::Handler<Left> for Tui {
    type Return = ();
    async fn handle(&mut self, _message: Left, _ctx: &mut xtra::Context<Self>) -> Self::Return {
        match self.state.pages.last_mut() {
            Some(Page::Posts(posts_state)) => {
                posts_state
                    .handle(posts::MovePickedRel(-1, 0), &self.ctx)
                    .await;
            }
            Some(Page::Post(post_state)) => {
                post_state
                    .handle(post::ChangeSplitRatioInDiff(-1), &self.ctx)
                    .await;
            }
            _ => {}
        }
    }
}

impl xtra::Handler<Right> for Tui {
    type Return = ();

    async fn handle(&mut self, _message: Right, _ctx: &mut xtra::Context<Self>) -> Self::Return {
        match self.state.pages.last_mut() {
            Some(Page::Posts(posts_state)) => {
                posts_state
                    .handle(posts::MovePickedRel(1, 0), &self.ctx)
                    .await;
            }
            Some(Page::Post(post_state)) => {
                post_state
                    .handle(post::ChangeSplitRatioInDiff(1), &self.ctx)
                    .await;
            }
            _ => {}
        }
    }
}

pub struct NextPage;

pub struct PrevPage;

pub struct ToPage(pub u32);

pub struct Refresh;

pub struct Search(pub Vec<String>);

impl xtra::Handler<NextPage> for Tui {
    type Return = ();

    async fn handle(&mut self, _message: NextPage, _ctx: &mut xtra::Context<Self>) -> Self::Return {
        match self.state.pages.last_mut() {
            Some(Page::Posts(posts_state)) => {
                posts_state.handle(NextPage, &self.ctx).await;
            }
            _ => {}
        }
    }
}

impl xtra::Handler<PrevPage> for Tui {
    type Return = ();

    async fn handle(&mut self, _message: PrevPage, _ctx: &mut xtra::Context<Self>) -> Self::Return {
        match self.state.pages.last_mut() {
            Some(Page::Posts(posts_state)) => {
                posts_state.handle(PrevPage, &self.ctx).await;
            }
            _ => {}
        }
    }
}

impl xtra::Handler<ToPage> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        ToPage(page_no): ToPage,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        match self.state.pages.last_mut() {
            Some(Page::Posts(posts_state)) => {
                posts_state
                    .handle(posts::PostsReq(page_no as usize), &self.ctx)
                    .await;
            }
            _ => {}
        }
    }
}

impl xtra::Handler<Refresh> for Tui {
    type Return = ();

    async fn handle(&mut self, _message: Refresh, _ctx: &mut xtra::Context<Self>) -> Self::Return {
        match self.state.pages.last_mut() {
            Some(Page::Posts(posts_state)) => {
                posts_state.handle(Refresh, &self.ctx).await;
            }
            _ => {}
        }
    }
}

impl xtra::Handler<Search> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        Search(tags): Search,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        match self.state.pages.last_mut() {
            Some(Page::Posts(posts_state)) => {
                posts_state.handle(posts::UpdateTags(tags), &self.ctx).await;
            }
            _ => {}
        }
    }
}

#[derive(EnumAsInner)]
enum KeyPressesToCommand {
    Success,
    Partial,
    Failed,
}

fn digits_to_u32(chars: &[KeyPress]) -> u32 {
    chars.iter().fold(0, |acc, cur| {
        acc * 10 + cur.to_digit(10).unwrap_or_default()
    })
}

async fn key_presses_to_command(
    tui: &mut Tui,
    ctx: &mut xtra::Context<Tui>,
) -> KeyPressesToCommand {
    use KeyCode::*;
    match tui.state.key_presses.as_slice() {
        digits if digits.iter().all(|d| d.is_digit(10)) => KeyPressesToCommand::Partial,

        [KeyPress(KeyModifiers::NONE, Char('q'))] => {
            tui.handle(motions::Exit, ctx).await;
            KeyPressesToCommand::Success
        }

        [KeyPress(KeyModifiers::NONE, Char('i'))] => match tui.state.pages.last() {
            Some(Page::Posts(_) | Page::Post(_)) => {
                tui.handle(motions::Right, ctx).await;
                KeyPressesToCommand::Success
            }
            _ => KeyPressesToCommand::Failed,
        },

        [KeyPress(KeyModifiers::NONE, Char('h'))] => match tui.state.pages.last() {
            Some(Page::Posts(_) | Page::Post(_)) => {
                tui.handle(motions::Left, ctx).await;
                KeyPressesToCommand::Success
            }
            _ => KeyPressesToCommand::Failed,
        },

        [KeyPress(KeyModifiers::NONE, Char('n'))] => match tui.state.pages.last() {
            Some(Page::Posts(_) | Page::Post(_)) => {
                tui.handle(motions::Down, ctx).await;
                KeyPressesToCommand::Success
            }
            _ => KeyPressesToCommand::Failed,
        },

        [KeyPress(KeyModifiers::NONE, Char('e'))] => match tui.state.pages.last() {
            Some(Page::Posts(_) | Page::Post(_)) => {
                tui.handle(motions::Up, ctx).await;
                KeyPressesToCommand::Success
            }
            _ => KeyPressesToCommand::Failed,
        },

        [KeyPress(KeyModifiers::NONE, Char('g')), KeyPress(KeyModifiers::NONE, Char('f'))] => {
            match tui.state.pages.last() {
                Some(Page::Posts(_) | Page::Post(_)) => {
                    tui.handle(posts_actions::Favorites, ctx).await;
                    KeyPressesToCommand::Success
                }
                _ => KeyPressesToCommand::Failed,
            }
        }

        [KeyPress(KeyModifiers::NONE, Char('f'))] => match tui.state.pages.last() {
            Some(Page::Posts(_) | Page::Post(_)) => {
                tui.handle(post_actions::FavoriteByContext, ctx).await;
                KeyPressesToCommand::Success
            }
            _ => KeyPressesToCommand::Failed,
        },

        [KeyPress(KeyModifiers::SHIFT, Char('F'))] => match tui.state.pages.last() {
            Some(Page::Posts(_) | Page::Post(_)) => {
                tui.handle(post_actions::UnfavoriteByContext, ctx).await;
                KeyPressesToCommand::Success
            }
            _ => KeyPressesToCommand::Failed,
        },

        [digits @ .., KeyPress(KeyModifiers::NONE, Char('g'))]
            if digits.iter().all(|d| d.is_digit(10)) =>
        {
            KeyPressesToCommand::Partial
        }

        [KeyPress(KeyModifiers::NONE, Char('g')), KeyPress(KeyModifiers::NONE, Char('p'))] => {
            match tui.state.pages.last() {
                Some(Page::Posts(_) | Page::Post(_)) => {
                    tui.handle(
                        posts_actions::ToPosts {
                            tags: Default::default(),
                        },
                        ctx,
                    )
                    .await;
                    KeyPressesToCommand::Success
                }
                _ => KeyPressesToCommand::Failed,
            }
        }

        [KeyPress(KeyModifiers::NONE, Char('/'))] => match tui.state.pages.last() {
            Some(Page::Posts(_)) => {
                tui.handle(prompt::StartInput { header: "/".into() }, ctx)
                    .await;
                KeyPressesToCommand::Success
            }
            _ => KeyPressesToCommand::Failed,
        },

        [KeyPress(KeyModifiers::NONE, Char(':'))] => {
            tui.handle(prompt::StartInput { header: ":".into() }, ctx)
                .await;
            KeyPressesToCommand::Success
        }

        [digits @ .., KeyPress(KeyModifiers::NONE, Char('g')), KeyPress(KeyModifiers::NONE, Char('t'))]
            if digits.iter().all(|d| d.is_digit(10)) =>
        {
            match tui.state.pages.last() {
                Some(Page::Posts(_)) => {
                    if digits.is_empty() {
                        tui.handle(motions::NextPage, ctx).await;
                    } else {
                        tui.handle(motions::ToPage(digits_to_u32(digits)), ctx)
                            .await;
                    }

                    KeyPressesToCommand::Success
                }
                _ => KeyPressesToCommand::Failed,
            }
        }

        [digits @ .., KeyPress(KeyModifiers::NONE, Char('g')), KeyPress(KeyModifiers::SHIFT, Char('T'))]
            if digits.iter().all(|d| d.is_digit(10)) =>
        {
            match tui.state.pages.last() {
                Some(Page::Posts(_)) => {
                    if digits.is_empty() {
                        tui.handle(motions::PrevPage, ctx).await;
                    } else {
                        tui.handle(motions::ToPage(digits_to_u32(digits)), ctx)
                            .await;
                    }

                    KeyPressesToCommand::Success
                }
                _ => KeyPressesToCommand::Failed,
            }
        }

        [KeyPress(KeyModifiers::NONE, Enter)] => match tui.state.pages.last() {
            Some(Page::Posts(_)) => {
                tui.handle(motions::Confirm, ctx).await;
                KeyPressesToCommand::Success
            }
            _ => KeyPressesToCommand::Failed,
        },

        [KeyPress(KeyModifiers::NONE, Right)] => {
            tui.handle(motions::Right, ctx).await;
            KeyPressesToCommand::Success
        }

        [KeyPress(KeyModifiers::NONE, Left)] => {
            tui.handle(motions::Left, ctx).await;
            KeyPressesToCommand::Success
        }

        [KeyPress(KeyModifiers::NONE, Down)] => {
            tui.handle(motions::Down, ctx).await;
            KeyPressesToCommand::Success
        }

        [KeyPress(KeyModifiers::NONE, Up)] => {
            tui.handle(motions::Up, ctx).await;
            KeyPressesToCommand::Success
        }

        _ => KeyPressesToCommand::Failed,
    }
}

impl xtra::Handler<KeyPress> for Tui {
    type Return = ();

    #[tracing::instrument(skip(self, ctx))]
    async fn handle(&mut self, key_press: KeyPress, ctx: &mut xtra::Context<Self>) -> Self::Return {
        tracing::debug!("entry");
        if self.state.prompt.can_handle_key_event() {
            self.handle(prompt::PromptKeyPress(key_press), ctx).await;
            return;
        }

        self.state.key_presses.push(key_press);
        let mut result = key_presses_to_command(self, ctx).await;

        if result.is_failed() && self.state.key_presses.len() > 1 {
            let key_press = self.state.key_presses.pop().unwrap();
            self.state.key_presses.clear();
            tracing::debug!("retry {key_press:?} with partial_key emptied");
            self.state.key_presses.push(key_press);
            result = key_presses_to_command(self, ctx).await;
        }

        self.handle(
            KeyPressHint(Some(
                self.state
                    .key_presses
                    .iter()
                    .map(|press| press.to_string())
                    .collect::<Vec<_>>()
                    .join(""),
            )),
            ctx,
        )
        .await;

        match &result {
            KeyPressesToCommand::Partial => {}
            KeyPressesToCommand::Success | KeyPressesToCommand::Failed => {
                self.state.key_presses.clear();
                self.handle(KeyPressHintTimeoutClear(Duration::from_millis(80)), ctx)
                    .await;
            }
        }
    }
}

struct KeyPressHint(pub Option<String>);

struct KeyPressHintTimeoutClear(pub Duration);

impl xtra::Handler<KeyPressHint> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        KeyPressHint(hint): KeyPressHint,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        match hint {
            Some(next_key_hint) if next_key_hint != self.state.key_hint => {
                self.state.key_hint = next_key_hint;
                self.ctx.render_manager.request().await;
            }

            None if !self.state.key_hint.is_empty() => {
                self.state.key_hint.clear();
                self.ctx.render_manager.request().await;
            }

            _ => {}
        }
    }
}

impl xtra::Handler<KeyPressHintTimeoutClear> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        KeyPressHintTimeoutClear(duration): KeyPressHintTimeoutClear,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        if let Some(handle) = &self.state.key_hint_clear_handle {
            handle.abort();
        }

        let tui_address = self.ctx.tui_address.clone();
        let next_handle = tokio::spawn(async move {
            tokio::time::sleep(duration).await;
            let _ = tui_address.send(KeyPressHint(None)).detach().await;
        })
        .abort_handle();

        self.state.key_hint_clear_handle = Some(next_handle);
    }
}
