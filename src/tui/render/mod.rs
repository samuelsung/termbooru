use super::{pages::Page, State};
use ratatui::prelude::*;

pub mod handler;

fn render(state: &mut State, frame: &mut Frame) {
    tracing::trace!("start rendering tui");
    let layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(vec![
            Constraint::Percentage(100),
            Constraint::Length(1),
            Constraint::Length(1),
        ])
        .split(frame.area());

    use Page::*;
    match state.pages.last_mut() {
        Some(Posts(posts_state)) => posts_state.render(frame, layout[0]),
        Some(Post(post_state)) => post_state.render(frame, layout[0]),
        None => {}
    }

    let page_info = state
        .pages
        .iter()
        .rev()
        .take(4)
        .enumerate()
        .rev()
        .map(|(i, page)| {
            if i == 3 {
                return "..".into();
            }

            match page {
                Posts(posts_state) => posts_state.page_info(),
                Post(post_state) => post_state.page_info(),
            }
        })
        .collect::<Vec<_>>()
        .join(" > ");

    frame.render_widget(Text::from(page_info).right_aligned(), layout[1]);

    frame.render_widget(
        Text::from(state.key_hint.as_str()).right_aligned(),
        layout[2],
    );

    state.prompt.render(frame, layout[2]);

    state.prompt.render_autocomplete(frame, layout[1]);

    tracing::trace!("done rendering tui");
}
