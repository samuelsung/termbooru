use std::{
    io,
    sync::atomic::{AtomicU32, Ordering},
};

use derive_debug::Dbg;
use xtra::Address;

use crate::tui::Tui;

use super::render;

#[derive(Clone, Copy, Debug)]
struct Render;

impl xtra::Handler<Render> for Tui {
    type Return = io::Result<()>;

    #[tracing::instrument(skip(self, _ctx))]
    async fn handle(
        &mut self,
        _message: Render,
        _ctx: &mut xtra::prelude::Context<Self>,
    ) -> Self::Return {
        tracing::trace!("render....");
        self.ctx.render_manager.clear();
        match self.terminal.draw(|frame| render(&mut self.state, frame)) {
            Ok(_) => return Ok(()),
            Err(err) => {
                tracing::error!("render error: {err:?}");
            }
        }

        Ok(())
    }
}

#[derive(Dbg)]
pub struct RenderManager {
    request_count: AtomicU32,
    #[dbg(skip)]
    tui_address: Address<Tui>,
}

impl RenderManager {
    pub fn from_address(tui_address: Address<Tui>) -> Self {
        Self {
            request_count: AtomicU32::new(0),
            tui_address,
        }
    }

    #[tracing::instrument]
    pub async fn request(&self) {
        let previous = self.request_count.fetch_add(1, Ordering::AcqRel);
        if previous == 0 {
            tracing::debug!("request new render");
            let _ = self.tui_address.send(Render).priority(0).detach().await;
        } else {
            tracing::debug!("skip render");
        }
    }

    #[tracing::instrument]
    pub fn clear(&self) {
        tracing::debug!("clear request count");
        self.request_count.store(0, Ordering::Release);
    }
}
