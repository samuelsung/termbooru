use std::sync::Arc;

use rdanbooru::{
    apis::{delete_favorite_by_post_id, get_post_by_id, post_favorite_by_post_id},
    models::post::Post,
    ClientError,
};

use crate::{
    pages::{
        post::{PostState, PostStateBuildError, PostStateBuilder},
        posts, prompt,
    },
    remotes::Request,
};

use super::{pages::Page, SubHandler, Tui};

pub struct FavoriteByContext;

pub struct FavoriteByPostId(pub usize);

struct FavoriteByPostIdSuccess(pub Post);

struct FavoriteByPostIdFailure(pub usize, pub ClientError);

impl xtra::Handler<FavoriteByContext> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        _message: FavoriteByContext,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let post_id = match self.state.pages.last() {
            Some(Page::Post(post_state)) => Some(post_state.post.id),
            Some(Page::Posts(posts_state)) => posts_state.picked_post().map(|p| p.id),
            _ => None,
        };

        if let Some(post_id) = post_id {
            self.handle(FavoriteByPostId(post_id), ctx).await;
        } else {
            self.handle(
                prompt::Echo("[Warning] post to favorite is undefined in this context".into()),
                ctx,
            )
            .await;
        }
    }
}

impl xtra::Handler<FavoriteByPostId> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        FavoriteByPostId(post_id): FavoriteByPostId,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let remote_manager_address = self.ctx.remote_manager_address.clone();
        let tui_address = self.ctx.tui_address.clone();

        tokio::spawn(async move {
            let result = remote_manager_address
                .send(Request::from(post_favorite_by_post_id(post_id)))
                .await
                .expect("Request manager should not be dropped")
                .await
                .expect("Tokio spawn panic");

            match result {
                Ok(post) => {
                    let _ = tui_address
                        .send(FavoriteByPostIdSuccess(post))
                        .detach()
                        .await;
                }
                Err(err) => {
                    let _ = tui_address
                        .send(FavoriteByPostIdFailure(post_id, err))
                        .detach()
                        .await;
                }
            }
        });

        self.handle(
            prompt::Echo(format!("[Request] favorite post {post_id}")),
            ctx,
        )
        .await;
    }
}

impl xtra::Handler<FavoriteByPostIdSuccess> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        FavoriteByPostIdSuccess(post): FavoriteByPostIdSuccess,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let post_id = post.id;
        let arc_post = Arc::new(post.clone()); // TODO(samuelsung): not clone() the object

        for page in self.state.pages.iter_mut() {
            match page {
                Page::Post(ref mut post_state) if post_state.post.id == post.id => {
                    post_state.is_favorited = true;
                    post_state.post = post.clone();
                }
                Page::Posts(posts_state) => {
                    posts_state
                        .handle(posts::OnPostUpdated(arc_post.clone()), &self.ctx)
                        .await;
                }
                _ => {}
            }
        }

        self.handle(
            prompt::Echo(format!("[Success] favorited post {post_id}")),
            ctx,
        )
        .await;

        self.ctx.render_manager.request().await;
    }
}

impl xtra::Handler<FavoriteByPostIdFailure> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        FavoriteByPostIdFailure(post_id, err): FavoriteByPostIdFailure,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!("[Error] failed to favorite post {post_id}: {err}")),
            ctx,
        )
        .await;
    }
}

pub struct UnfavoriteByContext;

pub struct UnfavoriteByPostId(pub usize);

struct UnfavoriteByPostIdSuccess(pub Post);

struct UnfavoriteByPostIdFailure(pub usize, pub ClientError);

impl xtra::Handler<UnfavoriteByContext> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        _message: UnfavoriteByContext,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let post_id = match self.state.pages.last() {
            Some(Page::Post(post_state)) => Some(post_state.post.id),
            Some(Page::Posts(posts_state)) => posts_state.picked_post().map(|p| p.id),
            _ => None,
        };

        if let Some(post_id) = post_id {
            self.handle(UnfavoriteByPostId(post_id), ctx).await;
        } else {
            self.handle(
                prompt::Echo("[Warning] post to unfavorite is undefined in this context".into()),
                ctx,
            )
            .await;
        }
    }
}

impl xtra::Handler<UnfavoriteByPostId> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        UnfavoriteByPostId(post_id): UnfavoriteByPostId,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let remote_manager_address = self.ctx.remote_manager_address.clone();
        let tui_address = self.ctx.tui_address.clone();

        tokio::spawn(async move {
            let res = async move {
                remote_manager_address
                    .send(Request::from(delete_favorite_by_post_id(post_id)))
                    .await
                    .expect("Request manager should not be dropped")
                    .await
                    .expect("Tokio spawn panic")?;

                remote_manager_address
                    .send(Request::from(get_post_by_id(post_id)))
                    .await
                    .expect("Request manager should not be dropped")
                    .await
                    .expect("Tokio spawn panic")
            }
            .await;

            match res {
                Ok(post) => {
                    let _ = tui_address
                        .send(UnfavoriteByPostIdSuccess(post))
                        .detach()
                        .await;
                }
                Err(err) => {
                    let _ = tui_address
                        .send(UnfavoriteByPostIdFailure(post_id, err))
                        .detach()
                        .await;
                }
            };
        });

        self.handle(
            prompt::Echo(format!("[Request] unfavorite post {post_id}")),
            ctx,
        )
        .await;
    }
}

impl xtra::Handler<UnfavoriteByPostIdSuccess> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        UnfavoriteByPostIdSuccess(post): UnfavoriteByPostIdSuccess,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let post_id = post.id;
        let arc_post = Arc::new(post.clone());

        for page in self.state.pages.iter_mut() {
            match page {
                Page::Post(ref mut post_state) if post_state.post.id == post.id => {
                    post_state.is_favorited = false;
                    post_state.post = post.clone();
                }
                Page::Posts(posts_state) => {
                    posts_state
                        .handle(posts::OnPostUpdated(arc_post.clone()), &self.ctx)
                        .await;
                }
                _ => {}
            }
        }

        self.handle(
            prompt::Echo(format!("[Success] un-favorited post {post_id}")),
            ctx,
        )
        .await;

        self.ctx.render_manager.request().await;
    }
}

impl xtra::Handler<UnfavoriteByPostIdFailure> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        UnfavoriteByPostIdFailure(post_id, err): UnfavoriteByPostIdFailure,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!(
                "[Error] failed to un-favorite post {post_id}: {err}"
            )),
            ctx,
        )
        .await;
    }
}

pub struct ToPostByPostId(pub usize);

pub struct ToPostByBuilder(pub PostStateBuilder);

struct ToPostByBuilderSuccess(pub PostState);

struct ToPostByBuilderFailure(pub Option<usize>, pub PostStateBuildError);

impl xtra::Handler<ToPostByPostId> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        ToPostByPostId(post_id): ToPostByPostId,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            ToPostByBuilder(PostStateBuilder::default().post_id(post_id)),
            ctx,
        )
        .await;
    }
}

impl xtra::Handler<ToPostByBuilder> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        ToPostByBuilder(post_state_builder): ToPostByBuilder,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let post_id = post_state_builder.get_post_id();
        let user_id = self.state.profile.as_ref().and_then(|p| p.id);
        let self_ctx = self.ctx.clone();

        tokio::spawn(async move {
            match post_state_builder.build(user_id, &self_ctx).await {
                Ok(post_state) => {
                    let _ = self_ctx
                        .tui_address
                        .send(ToPostByBuilderSuccess(post_state))
                        .detach()
                        .await;
                }
                Err(err) => {
                    let _ = self_ctx
                        .tui_address
                        .send(ToPostByBuilderFailure(post_id, err))
                        .detach()
                        .await;
                }
            }
        });

        self.handle(
            prompt::Echo(format!(
                "[Request] fetch post {}",
                post_id.map(|id| id.to_string()).unwrap_or("?".to_string())
            )),
            ctx,
        )
        .await;
    }
}

impl xtra::Handler<ToPostByBuilderSuccess> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        ToPostByBuilderSuccess(post_state): ToPostByBuilderSuccess,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let post_id = post_state.post.id;

        self.state.pages.push(Page::Post(post_state));

        self.handle(
            prompt::Echo(format!("[Success] fetched post {post_id}")),
            ctx,
        )
        .await;

        self.ctx.render_manager.request().await;
    }
}

impl xtra::Handler<ToPostByBuilderFailure> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        ToPostByBuilderFailure(post_id, err): ToPostByBuilderFailure,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!(
                "[Error] failed to fetch post {}: {err}",
                post_id.map(|id| id.to_string()).unwrap_or("?".to_string())
            )),
            ctx,
        )
        .await;
    }
}
