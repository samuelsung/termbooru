use rdanbooru::{
    models::{auth::Auth, user::Profile},
    ClientError,
};

use crate::{
    pages::{post, prompt},
    remotes::{
        self,
        localstorage::{RwLoginError, AUTH_JSON},
    },
};

use super::{pages::Page, SubHandler, Tui};

pub struct Login {
    pub auth: Auth,
    pub save_on_success: bool,
}

struct LoginSuccess {
    #[allow(dead_code)]
    pub auth: Auth,
    pub profile: Profile,
}

struct LoginFailure(pub Auth, pub ClientError);

impl xtra::Handler<Login> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        Login {
            auth,
            save_on_success,
        }: Login,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!("[Request] login as {}", auth.login)),
            ctx,
        )
        .await;

        let remote_manager_address = self.ctx.remote_manager_address.clone();
        let tui_address = self.ctx.tui_address.clone();

        tokio::spawn(async move {
            let result = remote_manager_address
                .send(remotes::danbooru::Login(auth.clone()))
                .await
                .expect("Request manager should not be dropped")
                .await
                .expect("Tokio spawn panic");

            match result {
                Ok(profile) => {
                    if save_on_success {
                        let _ = tui_address.send(SaveLogin(auth.clone())).detach().await;
                    }

                    let _ = tui_address
                        .send(LoginSuccess { auth, profile })
                        .detach()
                        .await;
                }
                Err(err) => {
                    let _ = tui_address.send(LoginFailure(auth, err)).detach().await;
                }
            }
        });
    }
}

impl xtra::Handler<LoginSuccess> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        LoginSuccess { auth: _, profile }: LoginSuccess,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.state.profile = Some(profile);

        self.handle(
            prompt::Echo(format!(
                "[Success] Logged-in as user {}",
                self.state.profile.as_ref().unwrap().name
            )),
            ctx,
        )
        .await;

        for page in self.state.pages.iter_mut() {
            match page {
                Page::Posts(_) => {}
                Page::Post(post_state) => {
                    post_state
                        .handle(
                            post::OnLoginChangedReq(self.state.profile.as_ref().unwrap().id),
                            &self.ctx,
                        )
                        .await;
                }
            }
        }

        self.ctx.render_manager.request().await;
    }
}

impl xtra::Handler<LoginFailure> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        LoginFailure(auth, err): LoginFailure,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!("[Error] failed to login as {}: {err}", auth.login)),
            ctx,
        )
        .await;
    }
}

pub struct Logout;

impl xtra::Handler<Logout> for Tui {
    type Return = ();

    async fn handle(&mut self, _message: Logout, ctx: &mut xtra::Context<Self>) -> Self::Return {
        if self.state.profile.is_some() {
            self.state.profile = None;
            self.handle(prompt::Echo("[Success] logged-out".to_string()), ctx)
                .await;
        } else {
            self.handle(prompt::Echo("[Warning] you haven't login".to_string()), ctx)
                .await;
        }

        for page in self.state.pages.iter_mut() {
            match page {
                Page::Posts(_) => {}
                Page::Post(post_state) => {
                    post_state
                        .handle(post::OnLoginChangedReq(None), &self.ctx)
                        .await;
                }
            }
        }

        self.handle(RemoveLogin, ctx).await;
        self.ctx.render_manager.request().await;
    }
}

pub struct ReadLogin;

struct ReadLoginSuccess(pub Auth);

struct ReadLoginFailure(pub RwLoginError);

impl xtra::Handler<ReadLogin> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        _message: ReadLogin,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let remote_manager_address = self.ctx.remote_manager_address.clone();
        let tui_address = self.ctx.tui_address.clone();

        tokio::spawn(async move {
            let result = remote_manager_address
                .send(remotes::localstorage::ReadLogin)
                .await
                .expect("Request manager should not be dropped")
                .await
                .expect("Tokio spawn panic");

            match result {
                Ok(auth) => tui_address.send(ReadLoginSuccess(auth)).detach().await,
                Err(err) => tui_address.send(ReadLoginFailure(err)).detach().await,
            }
        });
    }
}

impl xtra::Handler<ReadLoginSuccess> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        ReadLoginSuccess(auth): ReadLoginSuccess,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            Login {
                auth,
                save_on_success: false,
            },
            ctx,
        )
        .await;
    }
}

impl xtra::Handler<ReadLoginFailure> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        ReadLoginFailure(err): ReadLoginFailure,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!(
                "[Warning] unable to find login info from {}, run as anonymous user: {err}",
                AUTH_JSON
            )),
            ctx,
        )
        .await;
    }
}

pub struct RemoveLogin;

struct RemoveLoginSuccess;

struct RemoveLoginFailure(pub RwLoginError);

impl xtra::Handler<RemoveLogin> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        _message: RemoveLogin,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let remote_manager_address = self.ctx.remote_manager_address.clone();
        let tui_address = self.ctx.tui_address.clone();

        tokio::spawn(async move {
            let result = remote_manager_address
                .send(remotes::localstorage::RemoveLogin)
                .await
                .expect("Request manager should not be dropped")
                .await
                .expect("Tokio spawn panic");

            match result {
                Ok(_) => tui_address.send(RemoveLoginSuccess).detach().await,
                Err(err) => tui_address.send(RemoveLoginFailure(err)).detach().await,
            }
        });
    }
}

impl xtra::Handler<RemoveLoginSuccess> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        _message: RemoveLoginSuccess,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!("[Success] removed {}", AUTH_JSON)),
            ctx,
        )
        .await;
    }
}

impl xtra::Handler<RemoveLoginFailure> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        RemoveLoginFailure(err): RemoveLoginFailure,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!("[Warning] failed to removed {}: {err}", AUTH_JSON)),
            ctx,
        )
        .await;
    }
}

pub struct SaveLogin(pub Auth);

struct SaveLoginSuccess(pub Auth);

struct SaveLoginFailure(pub Auth, pub RwLoginError);

impl xtra::Handler<SaveLogin> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        SaveLogin(auth): SaveLogin,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let remote_manager_address = self.ctx.remote_manager_address.clone();
        let tui_address = self.ctx.tui_address.clone();

        tokio::spawn(async move {
            let result = remote_manager_address
                .send(remotes::localstorage::SaveLogin(auth.clone()))
                .await
                .expect("Request manager should not be dropped")
                .await
                .expect("Tokio spawn panic");

            match result {
                Ok(_) => tui_address.send(SaveLoginSuccess(auth)).detach().await,
                Err(err) => tui_address.send(SaveLoginFailure(auth, err)).detach().await,
            }
        });
    }
}

impl xtra::Handler<SaveLoginSuccess> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        SaveLoginSuccess(auth): SaveLoginSuccess,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!(
                "[Success] saved login info of {} to {AUTH_JSON}",
                auth.login
            )),
            ctx,
        )
        .await;
    }
}

impl xtra::Handler<SaveLoginFailure> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        SaveLoginFailure(auth, err): SaveLoginFailure,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!(
                "[Warning] failed to save login info of {} to {AUTH_JSON}: {err}",
                auth.login
            )),
            ctx,
        )
        .await;
    }
}
