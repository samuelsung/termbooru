pub mod auth_actions;
pub mod commands;
pub mod image;
pub mod motions;
pub mod pages;
pub mod post_actions;
pub mod posts_actions;
pub mod render;

use std::{
    future::Future,
    io::{self, stdout, Stdout},
    sync::Arc,
};

use crossterm::{
    event::{EventStream, KeyEventKind},
    execute,
    terminal::*,
};
use futures::StreamExt;
use pages::{Page, PageIdGenerator};
use ratatui::prelude::*;
use ratatui_image::picker::Picker;
use rdanbooru::models::user::Profile;
use tokio::task::{AbortHandle, JoinHandle};
use xtra::Address;

use crate::{key_press::KeyPress, pages::prompt::Prompt, remotes::RemoteManager};

use self::render::handler::RenderManager;

#[derive(Clone)]
pub struct Ctx {
    pub image_picker: Arc<Picker>,
    pub page_id_generator: Arc<PageIdGenerator>,
    pub remote_manager_address: Address<RemoteManager>,
    pub render_manager: Arc<RenderManager>,
    pub tui_address: Address<Tui>,
}

#[derive(Debug, Default)]
pub struct State {
    pub key_hint: String,
    pub key_hint_clear_handle: Option<AbortHandle>,
    pub key_presses: Vec<KeyPress>,
    pub pages: Vec<Page>,
    pub profile: Option<Profile>,
    pub prompt: Prompt,
    pub size: (u16, u16),
}

pub struct Tui {
    pub ctx: Ctx,
    pub terminal: Terminal<CrosstermBackend<Stdout>>,
    pub state: State,
}

#[derive(Debug, thiserror::Error)]
pub enum TuiInitError {
    #[error("failed to read/write")]
    IO(#[from] io::Error),

    #[error("failed to read image")]
    ReadImage(#[from] ::image::ImageError),

    #[error("failed to read from terminal")]
    ReadTerminal(#[from] ratatui_image::errors::Errors),
}

impl Tui {
    pub fn try_new(
        remote_manager_address: Address<RemoteManager>,
        tui_address: Address<Tui>,
    ) -> Result<Self, TuiInitError> {
        let image_picker = Picker::from_query_stdio()?;

        let terminal = Terminal::new(CrosstermBackend::new(stdout()))?;

        let size = {
            let rect = terminal.size()?;
            (rect.width, rect.height)
        };

        // action_tx.try_send(Action::ReadLoginReq).unwrap();
        // if let Some(post_id) = &args.post {
        //     action_tx
        //         .try_send(Action::Command(Command::PostByPostId(*post_id)))
        //         .unwrap();
        // } else {
        //     action_tx
        //         .try_send(Action::Command(Command::Posts(args.tags)))
        //         .unwrap();
        // }

        spawn_crossterm_event_handler(tui_address.clone());

        let render_manager = Arc::new(RenderManager::from_address(tui_address.clone()));

        {
            let tui_address = tui_address.clone();
            let render_manager = render_manager.clone();

            tokio::spawn(async move {
                render_manager.request().await;
                let _ = tui_address.send(auth_actions::ReadLogin).detach().await;
                let _ = tui_address
                    .send(posts_actions::ToPosts { tags: vec![] })
                    .detach()
                    .await;
            });
        }

        Ok(Self {
            ctx: Ctx {
                image_picker: Arc::new(image_picker),
                page_id_generator: Arc::new(PageIdGenerator::default()),
                remote_manager_address,
                render_manager,
                tui_address,
            },
            terminal,
            state: State {
                size,
                ..State::default()
            },
        })
    }
}

impl xtra::Actor for Tui {
    type Stop = ();

    async fn started(&mut self, _ctx: &xtra::Mailbox<Self>) -> Result<(), Self::Stop> {
        execute!(stdout(), EnterAlternateScreen)
            .and_then(|_| enable_raw_mode())
            .and_then(|_| self.terminal.clear())
            .map_err(|_e| ())
    }

    async fn stopped(self) -> Self::Stop {
        execute!(stdout(), LeaveAlternateScreen)
            .and_then(|_| disable_raw_mode())
            .unwrap();
    }
}

pub trait SubHandler<M> {
    type Return: Send + 'static;

    fn handle(&mut self, message: M, ctx: &Ctx) -> impl Future<Output = Self::Return> + Send;
}

fn spawn_crossterm_event_handler(tui_address: Address<Tui>) -> JoinHandle<()> {
    tokio::spawn(async move {
        let mut reader = EventStream::new();

        loop {
            match reader.next().await {
                Some(Ok(event)) => {
                    // NOTE(samuelsung): should not cause error as action channel should not be
                    // closed
                    tui_address.send(event).await.unwrap();
                }
                Some(Err(e)) => eprintln!("{}", e),
                None => break,
            }
        }
    })
}

impl xtra::Handler<crossterm::event::Event> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        message: crossterm::event::Event,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        match message {
            // NOTE: it's important to check that the event is a key press event as
            // crossterm also emits key release and repeat events on Windows.
            crossterm::event::Event::Key(key_event) if key_event.kind == KeyEventKind::Press => {
                self.handle(KeyPress(key_event.modifiers, key_event.code), ctx)
                    .await;
            }

            crossterm::event::Event::Resize(col, row) => {
                self.handle(OnResize(col, row), ctx).await;
            }

            _ => {}
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct OnResize(pub u16, pub u16);

impl xtra::Handler<OnResize> for Tui {
    type Return = ();

    async fn handle(&mut self, message: OnResize, _ctx: &mut xtra::Context<Self>) -> Self::Return {
        let OnResize(col, row) = message;
        self.state.size = (col, row);

        for p in self.state.pages.iter_mut() {
            match p {
                Page::Posts(posts_state) => posts_state.handle(message, &self.ctx).await,
                _ => {}
            }
        }

        self.ctx.render_manager.request().await; // NOTE(samuelsung): resize event should not be skip rendering
    }
}
