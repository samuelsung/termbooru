use rdanbooru::models::auth::Auth;
use xtra::Handler;

use crate::{
    key_press::KeyPress,
    pages::prompt,
    tui::{self, auth_actions, motions, post_actions, posts_actions, Tui},
};

pub struct RawCommand(pub String);

impl Handler<RawCommand> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        RawCommand(str): RawCommand,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let parsed_text: Vec<&str> = str.split_whitespace().collect::<Vec<_>>();

        match &*parsed_text {
            ["confirm"] => self.handle(motions::Confirm, ctx).await,

            ["down"] => self.handle(motions::Down, ctx).await,

            ["echo", rest @ ..] => self.handle(prompt::Echo(rest.join(" ")), ctx).await,

            ["exit"] => self.handle(motions::Exit, ctx).await,

            ["favorite"] => self.handle(post_actions::FavoriteByContext, ctx).await,

            ["favorites"] => self.handle(posts_actions::Favorites, ctx).await,

            ["left"] => self.handle(motions::Left, ctx).await,

            ["login", login, api_key] => {
                self.handle(
                    auth_actions::Login {
                        auth: Auth {
                            login: login.to_string(),
                            api_key: api_key.to_string(),
                        },
                        save_on_success: true,
                    },
                    ctx,
                )
                .await
            }

            ["logout"] => self.handle(auth_actions::Logout, ctx).await,

            ["next_page"] => self.handle(motions::NextPage, ctx).await,

            ["post", post_id] => match post_id.parse() {
                Ok(post_id) => {
                    self.handle(post_actions::ToPostByPostId(post_id), ctx)
                        .await
                }
                Err(err) => {
                    self.handle(
                        prompt::Echo(format!(
                            "[Error] failed to parse post_id \"{post_id}\": {err}"
                        )),
                        ctx,
                    )
                    .await
                }
            },

            ["posts", rest @ ..] => {
                self.handle(
                    posts_actions::ToPosts {
                        tags: rest.iter().map(|x| x.to_string()).collect(),
                    },
                    ctx,
                )
                .await
            }

            ["prev_page"] => self.handle(motions::PrevPage, ctx).await,

            ["raw_key_press", key_press] => match key_press.parse::<KeyPress>() {
                Ok(key_press) => self.handle(key_press, ctx).await,
                Err(err) => {
                    self.handle(
                        prompt::Echo(format!(
                            "[Error] failed to parse key_press \"{key_press}\": {err}"
                        )),
                        ctx,
                    )
                    .await
                }
            },

            ["refresh"] => self.handle(motions::Refresh, ctx).await,

            ["resize", col, row] => match col
                .parse()
                .and_then(|col| row.parse().map(|row| (col, row)))
            {
                Ok((col, row)) => self.handle(tui::OnResize(col, row), ctx).await,
                Err(err) => {
                    self.handle(
                        prompt::Echo(format!(
                            "[Error] failed to parse size: \"{col}x{row}\": {err}"
                        )),
                        ctx,
                    )
                    .await
                }
            },

            ["right"] => self.handle(motions::Right, ctx).await,

            ["search", rest @ ..] => {
                self.handle(
                    motions::Search(rest.iter().map(|x| x.to_string()).collect()),
                    ctx,
                )
                .await
            }

            ["start_input_command", header] => {
                self.handle(
                    prompt::StartInput {
                        header: header.to_string(),
                    },
                    ctx,
                )
                .await
            }

            ["to_page", page_no] => match page_no.parse() {
                Ok(page_no) => self.handle(motions::ToPage(page_no), ctx).await,
                Err(err) => {
                    self.handle(
                        prompt::Echo(format!(
                            "[Error] failed to parse page number \"{page_no}\": {err}"
                        )),
                        ctx,
                    )
                    .await
                }
            },

            ["unfavorite"] => self.handle(post_actions::UnfavoriteByContext, ctx).await,

            ["up"] => self.handle(motions::Up, ctx).await,

            _ => {
                self.handle(prompt::Echo(format!("unknown command \"{str}\"")), ctx)
                    .await
            }
        }
    }
}
