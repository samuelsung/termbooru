use crate::pages::{
    posts::{PostsPageBuildError, PostsState},
    prompt,
};

use super::{pages::Page, Tui};

pub struct Favorites;

impl xtra::Handler<Favorites> for Tui {
    type Return = ();

    async fn handle(&mut self, _message: Favorites, ctx: &mut xtra::Context<Self>) -> Self::Return {
        if let Some(username) = self.state.profile.as_ref().map(|p| p.name.as_str()) {
            self.handle(
                ToPosts {
                    tags: vec![format!("fav:{username}")],
                },
                ctx,
            )
            .await;
        } else {
            self.handle(
                prompt::Echo("[Warning] You have to login before viewing favorites!".into()),
                ctx,
            )
            .await;
        }
    }
}

pub struct ToPosts {
    pub tags: Vec<String>,
}

struct ToPostsSuccess(pub PostsState);

struct ToPostsFailure(pub PostsPageBuildError);

impl xtra::Handler<ToPosts> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        ToPosts { tags }: ToPosts,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        let ctx = self.ctx.clone();
        let page_id = self.ctx.page_id_generator.gen_id();
        let size = self.state.size;
        let tui_address = self.ctx.tui_address.clone();

        tokio::spawn(async move {
            let res = PostsState::new(page_id, tags, size, &ctx).await;

            match res {
                Ok(posts_state) => {
                    let _ = tui_address.send(ToPostsSuccess(posts_state)).detach().await;
                }
                Err(err) => {
                    let _ = tui_address.send(ToPostsFailure(err)).detach().await;
                }
            }
        });
    }
}

impl xtra::Handler<ToPostsSuccess> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        ToPostsSuccess(posts_state): ToPostsSuccess,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!(
                "[Success] fetched posts /{}",
                posts_state.tags.join(" ")
            )),
            ctx,
        )
        .await;

        self.state.pages.push(Page::Posts(posts_state));
        self.ctx.render_manager.request().await;
    }
}

impl xtra::Handler<ToPostsFailure> for Tui {
    type Return = ();

    async fn handle(
        &mut self,
        ToPostsFailure(err): ToPostsFailure,
        ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        self.handle(
            prompt::Echo(format!("[Error] failed to fetch posts: {err}",)),
            ctx,
        )
        .await;
    }
}
