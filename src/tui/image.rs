use std::{fmt, sync::Arc};

use image::Rgba;
use ratatui::{buffer::Buffer, layout::Rect, widgets::StatefulWidget};
use ratatui_image::{protocol::StatefulProtocol, FilterType, Resize};
use tokio::sync::{Mutex, TryLockError};

use crate::tui::render::handler::RenderManager;

#[derive(Clone)]
pub struct AsyncStatefulProtocol {
    render_manager: Arc<RenderManager>,
    inner: Arc<Mutex<StatefulProtocol>>,
}

impl fmt::Debug for AsyncStatefulProtocol {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("AsyncStatefulProtocol")
            .field("render_counter", &"-")
            .field("inner", &"-")
            .finish()
    }
}

impl AsyncStatefulProtocol {
    pub fn new(render_manager: Arc<RenderManager>, inner: StatefulProtocol) -> Self {
        Self {
            render_manager,
            inner: Arc::new(Mutex::new(inner)),
        }
    }

    pub async fn try_resize(
        &mut self,
        resize: Resize,
        background_color: Rgba<u8>,
        area: Rect,
    ) -> Result<(), TryLockError> {
        let mut guard = self.inner.clone().try_lock_owned()?;
        let _ = tokio::task::spawn_blocking(move || {
            guard.resize_encode(&resize, background_color, area);
        })
        .await;
        Ok(())
    }
}

pub struct AsyncStatefulImage {
    resize: Resize,
    background_color: Rgba<u8>,
}

impl Default for AsyncStatefulImage {
    fn default() -> Self {
        Self::new()
    }
}

impl AsyncStatefulImage {
    pub fn new() -> Self {
        Self {
            resize: Resize::Fit(Some(FilterType::Lanczos3)),
            background_color: Rgba([0; 4]),
        }
    }

    #[allow(dead_code)]
    pub fn resize(mut self, resize: Resize) -> AsyncStatefulImage {
        self.resize = resize;
        self
    }

    #[allow(dead_code)]
    pub fn background_color(mut self, background_color: Rgba<u8>) -> AsyncStatefulImage {
        self.background_color = background_color;
        self
    }
}

impl StatefulWidget for AsyncStatefulImage {
    type State = AsyncStatefulProtocol;

    fn render(self, area: Rect, buf: &mut Buffer, state: &mut Self::State) {
        let guard = state.inner.clone().try_lock_owned();

        if let Ok(mut guard) = guard {
            if let Some(next_rect) = guard.needs_resize(&self.resize, area) {
                let render_counter = state.render_manager.clone();
                let resize = self.resize;

                tokio::task::spawn_blocking(move || {
                    guard.resize_encode(&resize, self.background_color, next_rect);
                    tokio::spawn(async move { render_counter.request().await })
                });
            } else if area.area() > 0 {
                guard.render(area, buf);
            }
        }
    }
}
