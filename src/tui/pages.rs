use std::{
    fmt,
    marker::PhantomData,
    sync::atomic::{AtomicUsize, Ordering},
};

use enum_as_inner::EnumAsInner;
use xtra::Handler;

use crate::pages::{post::PostState, posts::PostsState};

use super::{SubHandler, Tui};

impl fmt::Debug for PageId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.inner.fmt(f)
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
pub struct PageId {
    inner: usize,
}

#[derive(Debug, Default)]
pub struct PageIdGenerator {
    inner: AtomicUsize,
}

impl PageIdGenerator {
    pub fn gen_id(&self) -> PageId {
        let inner = self.inner.fetch_add(1, Ordering::AcqRel);
        PageId { inner }
    }
}

#[derive(Debug, EnumAsInner)]
pub enum Page {
    Posts(PostsState),
    Post(PostState),
}

impl Page {
    pub fn is_page_id(&self, page_id: PageId) -> bool {
        match self {
            Page::Posts(posts_state) => posts_state.page_id == page_id,
            Page::Post(post_state) => post_state.page_id == page_id,
        }
    }
}

pub struct PageMessage<M, State>(PageId, M, PhantomData<State>);

impl<M> PageMessage<M, PostState> {
    pub fn post(page_id: PageId, message: M) -> Self
    where
        PostState: SubHandler<M>,
    {
        Self(page_id, message, PhantomData)
    }
}

impl<M> PageMessage<M, PostsState> {
    pub fn posts(page_id: PageId, message: M) -> Self
    where
        PostsState: SubHandler<M>,
    {
        Self(page_id, message, PhantomData)
    }
}

impl<M> Handler<PageMessage<M, PostsState>> for Tui
where
    PostsState: SubHandler<M>,
    M: Send,
{
    type Return = ();

    async fn handle(
        &mut self,
        PageMessage(page_id, message, _): PageMessage<M, PostsState>,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        if let Some(Page::Posts(state)) = self
            .state
            .pages
            .iter_mut()
            .find(|page| page.is_page_id(page_id))
        {
            state.handle(message, &self.ctx).await;
        }
    }
}

impl<M> Handler<PageMessage<M, PostState>> for Tui
where
    PostState: SubHandler<M>,
    M: Send,
{
    type Return = ();

    async fn handle(
        &mut self,
        PageMessage(page_id, message, _): PageMessage<M, PostState>,
        _ctx: &mut xtra::Context<Self>,
    ) -> Self::Return {
        if let Some(Page::Post(state)) = self
            .state
            .pages
            .iter_mut()
            .find(|page| page.is_page_id(page_id))
        {
            state.handle(message, &self.ctx).await;
        }
    }
}
