use std::fmt;

use ratatui::style::Color;
use rdanbooru::models::{autocomplete::TagCategory, pool::PoolCategory, user::UserLevel};

pub struct HumanizedNumberCustom<M: fmt::Display, K: fmt::Display> {
    number: u32,
    million: M,
    thousand: K,
}

impl<M: fmt::Display, K: fmt::Display> fmt::Display for HumanizedNumberCustom<M, K> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let HumanizedNumberCustom {
            number,
            million,
            thousand,
        } = self;

        if *number >= 1_000_000 {
            write!(f, "{:.1}{million}", *number as f64 / 1_000_000.0)?;
        } else if *number >= 10_000 {
            write!(f, "{}{thousand}", *number / 1_000)?;
        } else if *number >= 1_000 {
            write!(f, "{:.1}{thousand}", *number as f64 / 1_000.0)?;
        } else {
            write!(f, "{}", self.number)?;
        }

        Ok(())
    }
}

impl<M: fmt::Display, K: fmt::Display> HumanizedNumberCustom<M, K> {
    pub fn new(number: u32, million: M, thousand: K) -> Self {
        Self {
            number,
            million,
            thousand,
        }
    }
}

pub struct HumanizedNumber {
    inner: HumanizedNumberCustom<&'static str, &'static str>,
}

impl From<u32> for HumanizedNumber {
    fn from(value: u32) -> Self {
        Self {
            inner: HumanizedNumberCustom::new(value, "M", "k"),
        }
    }
}

impl fmt::Display for HumanizedNumber {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.inner)
    }
}

pub fn pool_color(pool_category: &PoolCategory) -> Color {
    match pool_category {
        PoolCategory::Collection => Color::Blue,
        PoolCategory::Series => Color::Magenta,
    }
}

pub fn tag_color(tag_category: &TagCategory) -> Color {
    match tag_category {
        TagCategory::General => Color::Blue,
        TagCategory::Artist => Color::Red,
        TagCategory::Copyright => Color::Magenta,
        TagCategory::Character => Color::Green,
        TagCategory::Meta => Color::Yellow,
    }
}

pub fn user_level_color(user_level: &UserLevel) -> Color {
    match user_level {
        UserLevel::Restricted | UserLevel::Member => Color::Blue,
        UserLevel::Gold => Color::Yellow,
        UserLevel::Platinum => Color::Gray,
        UserLevel::Approver | UserLevel::Builder | UserLevel::Contributor => Color::Magenta,
        UserLevel::Moderator => Color::Green,
        UserLevel::Admin | UserLevel::Owner => Color::Red,
    }
}
