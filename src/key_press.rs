use std::fmt;

use crossterm::event::{KeyCode, KeyModifiers, MediaKeyCode, ModifierKeyCode};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct KeyPress(pub KeyModifiers, pub KeyCode);

impl From<char> for KeyPress {
    fn from(value: char) -> Self {
        Self::from_char(value)
    }
}

impl KeyPress {
    pub fn from_char(value: char) -> Self {
        KeyPress(KeyModifiers::NONE, KeyCode::Char(value))
    }

    #[allow(dead_code)]
    pub fn from_key_code(value: KeyCode) -> Self {
        KeyPress(KeyModifiers::NONE, value)
    }

    #[allow(dead_code)]
    pub fn from_fkey(value: u8) -> Self {
        KeyPress(KeyModifiers::NONE, KeyCode::F(value))
    }

    pub fn is_digit(&self, radix: u32) -> bool {
        if let KeyPress(_, KeyCode::Char(char)) = self {
            char.is_digit(radix)
        } else {
            false
        }
    }

    pub fn to_digit(&self, radix: u32) -> Option<u32> {
        if let KeyPress(_, KeyCode::Char(char)) = self {
            char.to_digit(radix)
        } else {
            None
        }
    }
}

pub struct SplitKeyPressStrIter<'s> {
    inner: &'s str,
    is_done: bool,
}

impl<'s> Iterator for SplitKeyPressStrIter<'s> {
    type Item = &'s str;

    fn next(&mut self) -> Option<Self::Item> {
        if self.is_done {
            return None;
        }

        let split = {
            let start_idx = if self.inner.starts_with('-') { 1 } else { 0 };

            self.inner
                .get(start_idx..)
                .and_then(|str| str.find(|c| c != '-'))
                .and_then(|idx| {
                    let idx = start_idx + idx;
                    self.inner
                        .get(idx..)
                        .and_then(|str| str.find('-'))
                        .map(|i| idx + i)
                })
                .map(|next_split_idx| self.inner.split_at(next_split_idx))
                .map(|(pre, sub)| (pre, sub.get(1..).unwrap_or("")))
        };

        if let Some((pre, sub)) = split {
            self.inner = sub;
            return Some(pre);
        }

        self.is_done = true;
        let next = self.inner;
        self.inner = "";
        return Some(next);
    }
}

impl<'s> DoubleEndedIterator for SplitKeyPressStrIter<'s> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.is_done {
            return None;
        }

        let split = {
            let start_idx = if self.inner.starts_with('-') { 1 } else { 0 };

            self.inner
                .get(start_idx..)
                .and_then(|str| str.rfind('-'))
                .and_then(|idx| {
                    self.inner
                        .get(start_idx..idx)
                        .and_then(|str| str.rfind(|c| c != '-'))
                        .map(|i| i + start_idx + 1)
                })
                .map(|next_split_idx| self.inner.split_at(next_split_idx))
                .map(|(pre, sub)| (pre, sub.get(1..).unwrap_or("")))
        };

        if let Some((pre, sub)) = split {
            self.inner = pre;
            return Some(sub);
        }

        self.is_done = true;
        let next = self.inner;
        self.inner = "";
        return Some(next);
    }
}

impl<'s> SplitKeyPressStrIter<'s> {
    pub fn from(str: &'s str) -> Self {
        Self {
            inner: str,
            is_done: false,
        }
    }
}

#[derive(Clone, Debug, thiserror::Error)]
pub enum ParseKeyPressError {
    #[error("Invalid Token: empty key")]
    Empty,

    #[error("Invalid Token: Non digit character in F Key")]
    NonDigitAfterFKey,

    #[error("Invalid Token: unknown key")]
    UnknownKey,

    #[error("Invalid Token: unknown key")]
    UnknownModifier,
}

impl std::str::FromStr for KeyPress {
    type Err = ParseKeyPressError;

    fn from_str(str: &str) -> Result<Self, Self::Err> {
        let stripped_str = str
            .strip_prefix('<')
            .and_then(|str| str.strip_suffix('>'))
            .unwrap_or(str);

        let mut splits = SplitKeyPressStrIter::from(stripped_str).rev();

        let code = match splits.next() {
            Some("BS") => KeyCode::Backspace,
            Some("CR") => KeyCode::Enter,
            Some("Left") => KeyCode::Left,
            Some("Right") => KeyCode::Right,
            Some("Up") => KeyCode::Up,
            Some("Down") => KeyCode::Down,
            Some("Home") => KeyCode::Home,
            Some("End") => KeyCode::End,
            Some("PageUp") => KeyCode::PageUp,
            Some("PageDown") => KeyCode::PageDown,
            Some("Tab") => KeyCode::Tab,
            Some("Del") => KeyCode::Delete,
            Some("Insert") => KeyCode::Insert,
            Some("Nul") => KeyCode::Null,
            Some("Esc") => KeyCode::Esc,
            Some("CapsLock") => KeyCode::CapsLock,
            Some("ScrollLock") => KeyCode::ScrollLock,
            Some("NumLock") => KeyCode::NumLock,
            Some("PrintScreen") => KeyCode::PrintScreen,
            Some("Pause") => KeyCode::Pause,
            Some("Menu") => KeyCode::Menu,
            Some("MediaPlay") => KeyCode::Media(MediaKeyCode::Play),
            Some("MediaPause") => KeyCode::Media(MediaKeyCode::Pause),
            Some("MediaPlayPause") => KeyCode::Media(MediaKeyCode::PlayPause),
            Some("MediaReverse") => KeyCode::Media(MediaKeyCode::Reverse),
            Some("MediaStop") => KeyCode::Media(MediaKeyCode::Stop),
            Some("MediaFastForward") => KeyCode::Media(MediaKeyCode::FastForward),
            Some("MediaRewind") => KeyCode::Media(MediaKeyCode::Rewind),
            Some("MediaTrackNext") => KeyCode::Media(MediaKeyCode::TrackNext),
            Some("MediaTrackPrevious") => KeyCode::Media(MediaKeyCode::TrackPrevious),
            Some("MediaRecord") => KeyCode::Media(MediaKeyCode::Record),
            Some("MediaLowerVolume") => KeyCode::Media(MediaKeyCode::LowerVolume),
            Some("MediaRaiseVolume") => KeyCode::Media(MediaKeyCode::RaiseVolume),
            Some("MediaMuteVolume") => KeyCode::Media(MediaKeyCode::MuteVolume),
            Some("LeftShift") => KeyCode::Modifier(ModifierKeyCode::LeftShift),
            Some("LeftControl") => KeyCode::Modifier(ModifierKeyCode::LeftControl),
            Some("LeftAlt") => KeyCode::Modifier(ModifierKeyCode::LeftAlt),
            Some("LeftSuper") => KeyCode::Modifier(ModifierKeyCode::LeftSuper),
            Some("LeftHyper") => KeyCode::Modifier(ModifierKeyCode::LeftHyper),
            Some("LeftMeta") => KeyCode::Modifier(ModifierKeyCode::LeftMeta),
            Some("RightShift") => KeyCode::Modifier(ModifierKeyCode::RightShift),
            Some("RightControl") => KeyCode::Modifier(ModifierKeyCode::RightControl),
            Some("RightAlt") => KeyCode::Modifier(ModifierKeyCode::RightAlt),
            Some("RightSuper") => KeyCode::Modifier(ModifierKeyCode::RightSuper),
            Some("RightHyper") => KeyCode::Modifier(ModifierKeyCode::RightHyper),
            Some("RightMeta") => KeyCode::Modifier(ModifierKeyCode::RightMeta),
            Some("IsoLevel3Shift") => KeyCode::Modifier(ModifierKeyCode::IsoLevel3Shift),
            Some("IsoLevel5Shift") => KeyCode::Modifier(ModifierKeyCode::IsoLevel5Shift),
            Some(str) => {
                let mut chars = str.chars();
                match chars.next() {
                    Some('F') => {
                        let mut number: Option<u8> = None;

                        while let Some(char) = chars.next() {
                            match char {
                                '0'..='9' => {
                                    if let Some(ref mut number) = number {
                                        *number = *number * 10 + char.to_digit(10).unwrap() as u8
                                    } else {
                                        number = Some(char.to_digit(10).unwrap() as u8)
                                    }
                                }
                                _ => return Err(ParseKeyPressError::NonDigitAfterFKey),
                            }
                        }

                        if let Some(number) = number {
                            KeyCode::F(number)
                        } else {
                            KeyCode::Char('F')
                        }
                    }
                    Some(char) => {
                        if let Some(_) = chars.next() {
                            return Err(ParseKeyPressError::UnknownKey);
                        } else {
                            KeyCode::Char(char)
                        }
                    }

                    None => return Err(ParseKeyPressError::Empty),
                }
            }
            None => return Err(ParseKeyPressError::Empty),
        };

        let mut modifiers = KeyModifiers::NONE;

        while let Some(str) = splits.next() {
            match str {
                "S" => modifiers.insert(KeyModifiers::SHIFT),
                "C" => modifiers.insert(KeyModifiers::CONTROL),
                "A" => modifiers.insert(KeyModifiers::ALT),
                "M" => modifiers.insert(KeyModifiers::META),
                "SU" => modifiers.insert(KeyModifiers::SUPER),
                "H" => modifiers.insert(KeyModifiers::HYPER),
                _ => return Err(ParseKeyPressError::UnknownModifier),
            }
        }

        Ok(KeyPress(modifiers, code))
    }
}

impl fmt::Display for KeyPress {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let KeyPress(modifiers, code) = self;

        if !matches!(self, KeyPress(KeyModifiers::NONE, KeyCode::Char(_))) {
            write!(f, "<")?;
        }

        if modifiers.contains(KeyModifiers::SHIFT) {
            write!(f, "S-")?;
        }

        if modifiers.contains(KeyModifiers::CONTROL) {
            write!(f, "C-")?;
        }

        if modifiers.contains(KeyModifiers::ALT) {
            write!(f, "A-")?;
        }

        if modifiers.contains(KeyModifiers::META) {
            write!(f, "M-")?;
        }

        if modifiers.contains(KeyModifiers::SUPER) {
            write!(f, "SU-")?;
        }

        if modifiers.contains(KeyModifiers::HYPER) {
            write!(f, "H-")?;
        }

        match code {
            KeyCode::Backspace => write!(f, "BS")?,
            KeyCode::Enter => write!(f, "CR")?,
            KeyCode::Left => write!(f, "Left")?,
            KeyCode::Right => write!(f, "Right")?,
            KeyCode::Up => write!(f, "Up")?,
            KeyCode::Down => write!(f, "Down")?,
            KeyCode::Home => write!(f, "Home")?,
            KeyCode::End => write!(f, "End")?,
            KeyCode::PageUp => write!(f, "PageUp")?,
            KeyCode::PageDown => write!(f, "PageDown")?,
            KeyCode::Tab => write!(f, "Tab")?,
            KeyCode::BackTab => write!(f, "Tab")?,
            KeyCode::Delete => write!(f, "Del")?,
            KeyCode::Insert => write!(f, "Insert")?,
            KeyCode::F(x) => write!(f, "F{x}")?,
            KeyCode::Char(char) => write!(f, "{char}")?,
            KeyCode::Null => write!(f, "Nul")?,
            KeyCode::Esc => write!(f, "Esc")?,
            KeyCode::CapsLock => write!(f, "CapsLock")?,
            KeyCode::ScrollLock => write!(f, "ScrollLock")?,
            KeyCode::NumLock => write!(f, "NumLock")?,
            KeyCode::PrintScreen => write!(f, "PrintScreen")?,
            KeyCode::Pause => write!(f, "Pause")?,
            KeyCode::Menu => write!(f, "Menu")?,
            KeyCode::KeypadBegin => write!(f, "KeypadBegin")?,
            KeyCode::Media(MediaKeyCode::Play) => write!(f, "MediaPlay")?,
            KeyCode::Media(MediaKeyCode::Pause) => write!(f, "MediaPause")?,
            KeyCode::Media(MediaKeyCode::PlayPause) => write!(f, "MediaPlayPause")?,
            KeyCode::Media(MediaKeyCode::Reverse) => write!(f, "MediaReverse")?,
            KeyCode::Media(MediaKeyCode::Stop) => write!(f, "MediaStop")?,
            KeyCode::Media(MediaKeyCode::FastForward) => write!(f, "MediaFastForward")?,
            KeyCode::Media(MediaKeyCode::Rewind) => write!(f, "MediaRewind")?,
            KeyCode::Media(MediaKeyCode::TrackNext) => write!(f, "MediaTrackNext")?,
            KeyCode::Media(MediaKeyCode::TrackPrevious) => write!(f, "MediaTrackPrevious")?,
            KeyCode::Media(MediaKeyCode::Record) => write!(f, "MediaRecord")?,
            KeyCode::Media(MediaKeyCode::LowerVolume) => write!(f, "MediaLowerVolume")?,
            KeyCode::Media(MediaKeyCode::RaiseVolume) => write!(f, "MediaRaiseVolume")?,
            KeyCode::Media(MediaKeyCode::MuteVolume) => write!(f, "MediaMuteVolume")?,
            KeyCode::Modifier(ModifierKeyCode::LeftShift) => write!(f, "LeftShift")?,
            KeyCode::Modifier(ModifierKeyCode::LeftControl) => write!(f, "LeftControl")?,
            KeyCode::Modifier(ModifierKeyCode::LeftAlt) => write!(f, "LeftAlt")?,
            KeyCode::Modifier(ModifierKeyCode::LeftSuper) => write!(f, "LeftSuper")?,
            KeyCode::Modifier(ModifierKeyCode::LeftHyper) => write!(f, "LeftHyper")?,
            KeyCode::Modifier(ModifierKeyCode::LeftMeta) => write!(f, "LeftMeta")?,
            KeyCode::Modifier(ModifierKeyCode::RightShift) => write!(f, "RightShift")?,
            KeyCode::Modifier(ModifierKeyCode::RightControl) => write!(f, "RightControl")?,
            KeyCode::Modifier(ModifierKeyCode::RightAlt) => write!(f, "RightAlt")?,
            KeyCode::Modifier(ModifierKeyCode::RightSuper) => write!(f, "RightSuper")?,
            KeyCode::Modifier(ModifierKeyCode::RightHyper) => write!(f, "RightHyper")?,
            KeyCode::Modifier(ModifierKeyCode::RightMeta) => write!(f, "RightMeta")?,
            KeyCode::Modifier(ModifierKeyCode::IsoLevel3Shift) => write!(f, "IsoLevel3Shift")?,
            KeyCode::Modifier(ModifierKeyCode::IsoLevel5Shift) => write!(f, "IsoLevel5Shift")?,
        }

        if !matches!(self, KeyPress(KeyModifiers::NONE, KeyCode::Char(_))) {
            write!(f, ">")?;
        }

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn split_key_press_iter() {
        assert_eq!(SplitKeyPressStrIter::from("a").collect::<Vec<_>>(), ["a"]);

        assert_eq!(
            SplitKeyPressStrIter::from("a-").collect::<Vec<_>>(),
            ["a", ""]
        );

        assert_eq!(SplitKeyPressStrIter::from("-").collect::<Vec<_>>(), ["-"]);

        assert_eq!(SplitKeyPressStrIter::from("-a").collect::<Vec<_>>(), ["-a"]);

        assert_eq!(
            SplitKeyPressStrIter::from("a-bcd").collect::<Vec<_>>(),
            ["a", "bcd"]
        );

        assert_eq!(
            SplitKeyPressStrIter::from("a--bcd-def-----ghi--").collect::<Vec<_>>(),
            ["a", "-bcd", "def", "----ghi", "-"]
        );

        assert_eq!(
            SplitKeyPressStrIter::from("a--bcd-def-----ghi--")
                .rev()
                .collect::<Vec<_>>(),
            ["-", "----ghi", "def", "-bcd", "a"]
        );

        assert_eq!(
            SplitKeyPressStrIter::from("-a--bcd").collect::<Vec<_>>(),
            ["-a", "-bcd"]
        );

        assert_eq!(
            SplitKeyPressStrIter::from("-a--bcd")
                .rev()
                .collect::<Vec<_>>(),
            ["-bcd", "-a"]
        );
    }

    #[test]
    fn parse_single_char() -> Result<(), ParseKeyPressError> {
        assert_eq!("a".parse::<KeyPress>()?, KeyPress::from_char('a'));

        assert_eq!("-".parse::<KeyPress>()?, KeyPress::from_char('-'));

        assert_eq!("<S-b>".parse::<KeyPress>()?, {
            let mut key_press = KeyPress::from_char('b');
            key_press.0.insert(KeyModifiers::SHIFT);
            key_press
        });

        assert_eq!("<S-->".parse::<KeyPress>()?, {
            let mut key_press = KeyPress::from_char('-');
            key_press.0.insert(KeyModifiers::SHIFT);
            key_press
        });

        assert_eq!("<M-SU-Z>".parse::<KeyPress>()?, {
            let mut key_press = KeyPress::from_char('Z');
            key_press.0.insert(KeyModifiers::SUPER);
            key_press.0.insert(KeyModifiers::META);
            key_press
        });

        assert_eq!("<M-SU-->".parse::<KeyPress>()?, {
            let mut key_press = KeyPress::from_char('-');
            key_press.0.insert(KeyModifiers::SUPER);
            key_press.0.insert(KeyModifiers::META);
            key_press
        });

        Ok(())
    }

    #[test]
    fn parse_fxx() -> Result<(), ParseKeyPressError> {
        assert_eq!("<F1>".parse::<KeyPress>()?, KeyPress::from_fkey(1));

        assert_eq!("<S-F10>".parse::<KeyPress>()?, {
            let mut key_press = KeyPress::from_fkey(10);
            key_press.0.insert(KeyModifiers::SHIFT);
            key_press
        });

        assert_eq!("<A-C-F5>".parse::<KeyPress>()?, {
            let mut key_press = KeyPress::from_fkey(5);
            key_press.0.insert(KeyModifiers::ALT);
            key_press.0.insert(KeyModifiers::CONTROL);
            key_press
        });

        Ok(())
    }

    #[test]
    fn parse_special_key() -> Result<(), ParseKeyPressError> {
        assert_eq!(
            "<CR>".parse::<KeyPress>()?,
            KeyPress::from_key_code(KeyCode::Enter)
        );

        assert_eq!("<S-BS>".parse::<KeyPress>()?, {
            let mut key_press = KeyPress::from_key_code(KeyCode::Backspace);
            key_press.0.insert(KeyModifiers::SHIFT);
            key_press
        });

        assert_eq!("<C-A-Up>".parse::<KeyPress>()?, {
            let mut key_press = KeyPress::from_key_code(KeyCode::Up);
            key_press.0.insert(KeyModifiers::ALT);
            key_press.0.insert(KeyModifiers::CONTROL);
            key_press
        });

        assert_eq!("<A-SU-Del>".parse::<KeyPress>()?, {
            let mut key_press = KeyPress::from_key_code(KeyCode::Delete);
            key_press.0.insert(KeyModifiers::ALT);
            key_press.0.insert(KeyModifiers::SUPER);
            key_press
        });

        Ok(())
    }
}
