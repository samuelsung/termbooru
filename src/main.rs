mod autocompletes;
mod key_press;
mod misc;
mod pages;
mod remotes;
mod tui;

use std::{io, path::PathBuf};

use clap::Parser;
use remotes::RemoteManager;
use tui::Tui;
use xtra::Mailbox;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// view specific post
    #[arg(short, long)]
    post: Option<usize>,

    /// view specific tags
    #[arg(long)]
    tags: Vec<String>,

    /// print debug log to _
    #[arg(long)]
    log: Option<PathBuf>,

    /// log env filter
    #[arg(long, default_value_t = String::from("termbooru=info"))]
    log_filter: String,

    /// viewing safebooru instead of danbooru
    #[arg(long, default_value_t = false)]
    safe: bool,
}

#[tokio::main]
async fn main() -> io::Result<()> {
    let args = Args::parse();

    if let Some(path) = &args.log {
        if let Some(parent) = path.parent() {
            std::fs::create_dir_all(parent)?;
        }

        let file = std::fs::File::options()
            .append(true)
            .create(true)
            .open(path)?;

        let subscriber = tracing_subscriber::fmt()
            .with_writer(file)
            .with_env_filter(args.log_filter.clone().as_str())
            .finish();
        tracing::subscriber::set_global_default(subscriber).unwrap(); // TODO(samuelsung): error handling
    }

    let (tui_address, tui_mailbox) = Mailbox::unbounded();
    let remote_manager_address = xtra::spawn_tokio(RemoteManager::new(), Mailbox::unbounded());
    let tui_address = xtra::spawn_tokio(
        Tui::try_new(remote_manager_address, tui_address.clone()).unwrap(),
        (tui_address, tui_mailbox),
    );

    tui_address.join().await;

    Ok(())
}
