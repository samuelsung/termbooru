{
  description = "browse danbooru in the terminal";

  inputs = {
    devenv.url = "github:cachix/devenv";
    flake-parts.url = "github:hercules-ci/flake-parts";
    mk-shell-bin.url = "github:rrbutani/nix-mk-shell-bin";
    nixpkgs.url = "nixpkgs/nixos-unstable";
    nix2container.url = "github:nlewo/nix2container";
    nix2container.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; }
      {
        systems = [ "x86_64-linux" ];
        imports = [
          inputs.devenv.flakeModule
        ];
        perSystem = { pkgs, ... }:
          {
            devenv.shells.rust = {
              packages = [ pkgs.openssl ];
              languages.rust.enable = true;
            };
          };
      };
}
